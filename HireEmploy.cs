﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Final_Project_Dsa.BL;

namespace Final_Project_Dsa.Winforms
{
    public partial class HireEmploy : Form
    {
        HireEmployBL s1 = new HireEmployBL(18, "Shehroz@", "Shehroz", "Shehroz", "128", "Shehroz", "Shehroz", "Shehroz", 1899);
        static string ConnectionString = @"Data Source=.;Initial Catalog=Manager;Integrated Security=True";

        public HireEmploy()
        {
            InitializeComponent();
            loadData();
            loadData2();
        }
        void loadData()
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "SELECT * FROM tblEmployApplications";
                SqlCommand cmd = new SqlCommand(query, con);
                var reader = cmd.ExecuteReader();
                dataGridView1.Rows.Clear();
                while (reader.Read())
                {
                    dataGridView1.Rows.Add(reader["ID"], reader["FirstName"], reader["LastName"], reader["Address"], reader["PhoneNumber"], reader["Email"], reader["Password"], reader["HireDate"]);

                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }
        void loadData2()
        {
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM tblHireEmploys", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);

                dataGridView5.DataSource = dtbl;
                sqlCon.Close();
            }
        }
        void loadData1()
        {
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM tblRiderApplications", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);

                dataGridView1.DataSource = dtbl;
                sqlCon.Close();
            }
        }

        private void Hire_all_Load(object sender, EventArgs e)
        {

        }

        private void btn_hire_Click(object sender, EventArgs e)
        {
            hireEmployInfo();
            InsertData();
            DeleteEmploy();
            // SaveHireData();
            loadData();
            loadData2();
            clearData();
        }
        void clearData()
        {
            txt_id.Text = "";
            txt_salary.Text = "";
            txt_id1.Text = "";
        }
        void InsertData()
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("insert into tblHireEmploys values(@ID,@FirstName,@LastName,@Address,@PhoneNumber,@Email,@Password,@HireDate,@Salary)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ID", txt_id.Text);
                cmd.Parameters.AddWithValue("@FirstName", s1.firstname);
                cmd.Parameters.AddWithValue("@LastName", s1.lastname);
                cmd.Parameters.AddWithValue("@Address", s1.address);
                cmd.Parameters.AddWithValue("@PhoneNumber", s1.phonenumber);
                cmd.Parameters.AddWithValue("@Email", s1.email);
                cmd.Parameters.AddWithValue("@Password", s1.password);
                cmd.Parameters.AddWithValue("@HireDate", s1.hiredate);
                cmd.Parameters.AddWithValue("@Salary", s1.salary);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Hired");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        void SaveHireData()
        {
            string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
            SqlConnection Con = new SqlConnection(conString);
          //  Final_Project_Dsa.BL.EmployBL emp = new BL.EmployBL(int.Parse(txt_id.Text), txt_firstname.Text, txt_lastname.Text, txt_address.Text, txt_phonenumber.Text, txt_email.Text, txt_password.Text, txt_hiredate.Text);
            Con.Open();
            try
            {
                string q2 = "insert into tblHireEmploys (ID,FirstName,LastName,Address,PhoneNumber,Email,Password,HireDate,Salary) Values(" + s1.id + ",'" + s1.firstname + "','" + s1.lastname + "','" + s1.address + "'," + s1.phonenumber + ",'" + s1.email + "','" + s1.password + "','" + s1.hiredate + "','" + s1.salary +"')";
               // string q2 = "INSERT INTO tblHireEmploys (ID,FirstName,LastName,Address,PhoneNumber,Email,Password,HireDate,Salary) Values(s1.id ,s1.firstname , s1.lastname , s1.address, s1.phonenumber ,s1.email , s1.password ,s1.hiredate ,s1.salary )";
                SqlCommand cmd = new SqlCommand(q2, Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Hired", "MESSAGE");
                //clearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Failed to add");
            }
            Con.Close();
        }
        void DeleteEmploy()
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "DELETE FROM tblEmployApplications WHERE ID = " + s1.id;
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
                //MessageBox.Show("Rejected");
                // dataGridView1.Rows.Clear();
                loadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex);
            }
        }
        void hireEmployInfo()
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "SELECT * FROM tblEmployApplications WHERE ID = " + id1;
                SqlCommand cmd = new SqlCommand(query, con);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string firstname = reader["FirstName"].ToString();
                    string lastname = reader["LastName"].ToString();
                    string address = reader["Address"].ToString();
                    string  email= reader["Email"].ToString();
                    string password = reader["Password"].ToString();
                    string phonenumber = reader["PhoneNumber"].ToString();
                    //string salary = reader["Salary"].ToString();
                    string hiredate = reader["HireDate"].ToString();
                    string salary = txt_salary.Text;
                    s1 = new HireEmployBL(int.Parse(id1), firstname, lastname, address, phonenumber, email, password, hiredate, int.Parse(salary));

                }
                else
                {
                    MessageBox.Show("No record found");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }

        private void txt_id1_TextChanged(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 8 && e.RowIndex > -1)
            {
                txt_id.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(); 
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
        }

        private void btn_reject_Click(object sender, EventArgs e)
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "DELETE FROM tblEmployApplications WHERE ID = " + id1;
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Rejected");
               // dataGridView1.Rows.Clear();
                loadData();
                clearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex);
            }
        }

        private void btn_fire_Click(object sender, EventArgs e)
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "DELETE FROM tblRiderApplications WHERE ID = " + id1;
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Fired");
                //dataGridView5.Rows.Clear();
                loadData2();
                clearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex);
            }
        }

        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
