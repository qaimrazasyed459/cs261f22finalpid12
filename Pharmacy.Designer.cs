﻿
namespace Final_Project_Dsa
{
    partial class Pharmacy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slide_timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel_main = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Menu_pic = new System.Windows.Forms.PictureBox();
            this.homepanel = new System.Windows.Forms.Panel();
            this.btn_homepharma = new System.Windows.Forms.Button();
            this.purchasepanel = new System.Windows.Forms.Panel();
            this.btn_purchasepharma = new System.Windows.Forms.Button();
            this.returnpanel = new System.Windows.Forms.Panel();
            this.btn_return = new System.Windows.Forms.Button();
            this.exitpanel = new System.Windows.Forms.Panel();
            this.btn_exitpharma = new System.Windows.Forms.Button();
            this.lbl_Projectname = new System.Windows.Forms.Label();
            this.btn_panel = new System.Windows.Forms.Panel();
            this.panel_main.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).BeginInit();
            this.homepanel.SuspendLayout();
            this.purchasepanel.SuspendLayout();
            this.returnpanel.SuspendLayout();
            this.exitpanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // slide_timer2
            // 
            this.slide_timer2.Enabled = true;
            this.slide_timer2.Tick += new System.EventHandler(this.slide_timer2_Tick);
            // 
            // panel_main
            // 
            this.panel_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_main.Controls.Add(this.panel1);
            this.panel_main.Controls.Add(this.homepanel);
            this.panel_main.Controls.Add(this.purchasepanel);
            this.panel_main.Controls.Add(this.returnpanel);
            this.panel_main.Controls.Add(this.exitpanel);
            this.panel_main.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_main.Location = new System.Drawing.Point(0, 0);
            this.panel_main.MaximumSize = new System.Drawing.Size(185, 516);
            this.panel_main.MinimumSize = new System.Drawing.Size(68, 516);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(68, 516);
            this.panel_main.TabIndex = 17;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(80)))));
            this.panel1.Controls.Add(this.Menu_pic);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 76);
            this.panel1.TabIndex = 0;
            // 
            // Menu_pic
            // 
            this.Menu_pic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Menu_pic.Image = global::Final_Project_Dsa.Properties.Resources.noun_menu;
            this.Menu_pic.Location = new System.Drawing.Point(3, 23);
            this.Menu_pic.MaximumSize = new System.Drawing.Size(91, 70);
            this.Menu_pic.MinimumSize = new System.Drawing.Size(73, 50);
            this.Menu_pic.Name = "Menu_pic";
            this.Menu_pic.Size = new System.Drawing.Size(73, 50);
            this.Menu_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu_pic.TabIndex = 18;
            this.Menu_pic.TabStop = false;
            this.Menu_pic.Click += new System.EventHandler(this.Menu_pic_Click_2);
            // 
            // homepanel
            // 
            this.homepanel.Controls.Add(this.btn_homepharma);
            this.homepanel.Location = new System.Drawing.Point(3, 85);
            this.homepanel.Name = "homepanel";
            this.homepanel.Size = new System.Drawing.Size(182, 42);
            this.homepanel.TabIndex = 19;
            // 
            // btn_homepharma
            // 
            this.btn_homepharma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_homepharma.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_homepharma.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_homepharma.Image = global::Final_Project_Dsa.Properties.Resources.home_7_line;
            this.btn_homepharma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_homepharma.Location = new System.Drawing.Point(2, 4);
            this.btn_homepharma.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_homepharma.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_homepharma.Name = "btn_homepharma";
            this.btn_homepharma.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_homepharma.Size = new System.Drawing.Size(177, 35);
            this.btn_homepharma.TabIndex = 10;
            this.btn_homepharma.Text = "Home";
            this.btn_homepharma.UseVisualStyleBackColor = true;
            // 
            // purchasepanel
            // 
            this.purchasepanel.Controls.Add(this.btn_purchasepharma);
            this.purchasepanel.Location = new System.Drawing.Point(3, 133);
            this.purchasepanel.Name = "purchasepanel";
            this.purchasepanel.Size = new System.Drawing.Size(182, 43);
            this.purchasepanel.TabIndex = 19;
            // 
            // btn_purchasepharma
            // 
            this.btn_purchasepharma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_purchasepharma.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_purchasepharma.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_purchasepharma.Image = global::Final_Project_Dsa.Properties.Resources.shopping_cart_fill;
            this.btn_purchasepharma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_purchasepharma.Location = new System.Drawing.Point(3, 3);
            this.btn_purchasepharma.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_purchasepharma.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_purchasepharma.Name = "btn_purchasepharma";
            this.btn_purchasepharma.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_purchasepharma.Size = new System.Drawing.Size(177, 35);
            this.btn_purchasepharma.TabIndex = 14;
            this.btn_purchasepharma.Text = "Purchase";
            this.btn_purchasepharma.UseVisualStyleBackColor = true;
            // 
            // returnpanel
            // 
            this.returnpanel.Controls.Add(this.btn_return);
            this.returnpanel.Location = new System.Drawing.Point(3, 182);
            this.returnpanel.Name = "returnpanel";
            this.returnpanel.Size = new System.Drawing.Size(182, 42);
            this.returnpanel.TabIndex = 19;
            // 
            // btn_return
            // 
            this.btn_return.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_return.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_return.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_return.Image = global::Final_Project_Dsa.Properties.Resources.return2;
            this.btn_return.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_return.Location = new System.Drawing.Point(3, 4);
            this.btn_return.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_return.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_return.Name = "btn_return";
            this.btn_return.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_return.Size = new System.Drawing.Size(177, 35);
            this.btn_return.TabIndex = 13;
            this.btn_return.Text = "Return";
            this.btn_return.UseVisualStyleBackColor = true;
            this.btn_return.Click += new System.EventHandler(this.btn_return_Click);
            // 
            // exitpanel
            // 
            this.exitpanel.Controls.Add(this.btn_exitpharma);
            this.exitpanel.Location = new System.Drawing.Point(3, 230);
            this.exitpanel.Name = "exitpanel";
            this.exitpanel.Size = new System.Drawing.Size(182, 42);
            this.exitpanel.TabIndex = 20;
            // 
            // btn_exitpharma
            // 
            this.btn_exitpharma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exitpharma.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_exitpharma.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_exitpharma.Image = global::Final_Project_Dsa.Properties.Resources.Exit_3;
            this.btn_exitpharma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exitpharma.Location = new System.Drawing.Point(2, 3);
            this.btn_exitpharma.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_exitpharma.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_exitpharma.Name = "btn_exitpharma";
            this.btn_exitpharma.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btn_exitpharma.Size = new System.Drawing.Size(177, 35);
            this.btn_exitpharma.TabIndex = 16;
            this.btn_exitpharma.Text = "Exit";
            this.btn_exitpharma.UseVisualStyleBackColor = true;
            // 
            // lbl_Projectname
            // 
            this.lbl_Projectname.AllowDrop = true;
            this.lbl_Projectname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Projectname.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Projectname.Font = new System.Drawing.Font("Sitka Small", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Projectname.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_Projectname.Location = new System.Drawing.Point(161, 33);
            this.lbl_Projectname.MaximumSize = new System.Drawing.Size(570, 68);
            this.lbl_Projectname.MinimumSize = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.Name = "lbl_Projectname";
            this.lbl_Projectname.Size = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.TabIndex = 18;
            this.lbl_Projectname.Text = "Medicine Distribution Management";
            // 
            // btn_panel
            // 
            this.btn_panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.btn_panel.Location = new System.Drawing.Point(74, 82);
            this.btn_panel.Name = "btn_panel";
            this.btn_panel.Size = new System.Drawing.Size(628, 336);
            this.btn_panel.TabIndex = 19;
            this.btn_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.btn_panel_Paint_1);
            // 
            // Pharmacy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(714, 425);
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.btn_panel);
            this.Controls.Add(this.lbl_Projectname);
            this.MinimumSize = new System.Drawing.Size(730, 464);
            this.Name = "Pharmacy";
            this.Text = "Pharmacy";
            this.Load += new System.EventHandler(this.Pharmacy_Load);
            this.panel_main.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).EndInit();
            this.homepanel.ResumeLayout(false);
            this.purchasepanel.ResumeLayout(false);
            this.returnpanel.ResumeLayout(false);
            this.exitpanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer slide_timer2;
        private System.Windows.Forms.Button btn_return;
        private System.Windows.Forms.Button btn_exitpharma;
        private System.Windows.Forms.Button btn_purchasepharma;
        private System.Windows.Forms.Button btn_homepharma;
        private System.Windows.Forms.FlowLayoutPanel panel_main;
        private System.Windows.Forms.Label lbl_Projectname;
        private System.Windows.Forms.Panel homepanel;
        private System.Windows.Forms.Panel purchasepanel;
        private System.Windows.Forms.Panel returnpanel;
        private System.Windows.Forms.Panel exitpanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox Menu_pic;
        private System.Windows.Forms.Panel btn_panel;
    }
}