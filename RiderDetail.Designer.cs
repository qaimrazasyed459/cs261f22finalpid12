﻿namespace DSAProjectByShehroz
{
    partial class RiderDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_rider = new System.Windows.Forms.Panel();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.txt_salary = new System.Windows.Forms.TextBox();
            this.txt_phonenumber = new System.Windows.Forms.TextBox();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.lbl_salary = new System.Windows.Forms.Label();
            this.lbl_phonenumber = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.lbl_id = new System.Windows.Forms.Label();
            this.btn_show = new System.Windows.Forms.Button();
            this.txt_hiredate = new System.Windows.Forms.DateTimePicker();
            this.lbl_hiredate = new System.Windows.Forms.Label();
            this.panel_rider.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_rider
            // 
            this.panel_rider.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel_rider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_rider.Controls.Add(this.txt_hiredate);
            this.panel_rider.Controls.Add(this.lbl_hiredate);
            this.panel_rider.Controls.Add(this.btn_show);
            this.panel_rider.Controls.Add(this.txt_email);
            this.panel_rider.Controls.Add(this.txt_salary);
            this.panel_rider.Controls.Add(this.txt_phonenumber);
            this.panel_rider.Controls.Add(this.txt_address);
            this.panel_rider.Controls.Add(this.txt_name);
            this.panel_rider.Controls.Add(this.lbl_salary);
            this.panel_rider.Controls.Add(this.lbl_phonenumber);
            this.panel_rider.Controls.Add(this.lbl_email);
            this.panel_rider.Controls.Add(this.lbl_address);
            this.panel_rider.Controls.Add(this.lbl_name);
            this.panel_rider.Controls.Add(this.txt_id);
            this.panel_rider.Controls.Add(this.lbl_id);
            this.panel_rider.Location = new System.Drawing.Point(37, 38);
            this.panel_rider.Name = "panel_rider";
            this.panel_rider.Size = new System.Drawing.Size(635, 352);
            this.panel_rider.TabIndex = 0;
            // 
            // txt_email
            // 
            this.txt_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_email.Location = new System.Drawing.Point(193, 191);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(100, 83);
            this.txt_email.TabIndex = 29;
            // 
            // txt_salary
            // 
            this.txt_salary.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_salary.Location = new System.Drawing.Point(440, 147);
            this.txt_salary.Name = "txt_salary";
            this.txt_salary.Size = new System.Drawing.Size(100, 20);
            this.txt_salary.TabIndex = 27;
            // 
            // txt_phonenumber
            // 
            this.txt_phonenumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_phonenumber.Location = new System.Drawing.Point(440, 109);
            this.txt_phonenumber.Name = "txt_phonenumber";
            this.txt_phonenumber.Size = new System.Drawing.Size(100, 20);
            this.txt_phonenumber.TabIndex = 26;
            // 
            // txt_address
            // 
            this.txt_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_address.Location = new System.Drawing.Point(193, 147);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(100, 20);
            this.txt_address.TabIndex = 25;
            // 
            // txt_name
            // 
            this.txt_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_name.Location = new System.Drawing.Point(193, 109);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 20);
            this.txt_name.TabIndex = 24;
            // 
            // lbl_salary
            // 
            this.lbl_salary.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_salary.AutoSize = true;
            this.lbl_salary.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_salary.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_salary.Location = new System.Drawing.Point(334, 148);
            this.lbl_salary.Name = "lbl_salary";
            this.lbl_salary.Size = new System.Drawing.Size(57, 19);
            this.lbl_salary.TabIndex = 22;
            this.lbl_salary.Text = "Salary:";
            // 
            // lbl_phonenumber
            // 
            this.lbl_phonenumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_phonenumber.AutoSize = true;
            this.lbl_phonenumber.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_phonenumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_phonenumber.Location = new System.Drawing.Point(334, 110);
            this.lbl_phonenumber.Name = "lbl_phonenumber";
            this.lbl_phonenumber.Size = new System.Drawing.Size(109, 19);
            this.lbl_phonenumber.TabIndex = 21;
            this.lbl_phonenumber.Text = "Phone Number:";
            // 
            // lbl_email
            // 
            this.lbl_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_email.Location = new System.Drawing.Point(100, 190);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(53, 19);
            this.lbl_email.TabIndex = 20;
            this.lbl_email.Text = "Email:";
            // 
            // lbl_address
            // 
            this.lbl_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_address.Location = new System.Drawing.Point(100, 148);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(66, 19);
            this.lbl_address.TabIndex = 19;
            this.lbl_address.Text = "Address:";
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_name.Location = new System.Drawing.Point(100, 110);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(52, 19);
            this.lbl_name.TabIndex = 18;
            this.lbl_name.Text = "Name:";
            // 
            // txt_id
            // 
            this.txt_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_id.Location = new System.Drawing.Point(249, 28);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(100, 20);
            this.txt_id.TabIndex = 17;
            // 
            // lbl_id
            // 
            this.lbl_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_id.Location = new System.Drawing.Point(214, 29);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(29, 19);
            this.lbl_id.TabIndex = 16;
            this.lbl_id.Text = "ID:";
            // 
            // btn_show
            // 
            this.btn_show.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_show.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_show.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_show.Location = new System.Drawing.Point(383, 26);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(75, 27);
            this.btn_show.TabIndex = 30;
            this.btn_show.Text = "Show";
            this.btn_show.UseVisualStyleBackColor = true;
            // 
            // txt_hiredate
            // 
            this.txt_hiredate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_hiredate.Location = new System.Drawing.Point(299, 59);
            this.txt_hiredate.Name = "txt_hiredate";
            this.txt_hiredate.Size = new System.Drawing.Size(241, 20);
            this.txt_hiredate.TabIndex = 32;
            // 
            // lbl_hiredate
            // 
            this.lbl_hiredate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_hiredate.AutoSize = true;
            this.lbl_hiredate.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hiredate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_hiredate.Location = new System.Drawing.Point(216, 59);
            this.lbl_hiredate.Name = "lbl_hiredate";
            this.lbl_hiredate.Size = new System.Drawing.Size(77, 19);
            this.lbl_hiredate.TabIndex = 31;
            this.lbl_hiredate.Text = "Hire Date:";
            // 
            // RiderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(714, 425);
            this.Controls.Add(this.panel_rider);
            this.Name = "RiderDetail";
            this.Text = "RiderDetail";
            this.panel_rider.ResumeLayout(false);
            this.panel_rider.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_rider;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_salary;
        private System.Windows.Forms.TextBox txt_phonenumber;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label lbl_salary;
        private System.Windows.Forms.Label lbl_phonenumber;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.DateTimePicker txt_hiredate;
        private System.Windows.Forms.Label lbl_hiredate;
    }
}