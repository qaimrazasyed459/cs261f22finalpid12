﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq; 
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa.Winforms
{
    public partial class Product : Form
    {
        static string connection = "Data Source=.\\sqlexpress;Initial Catalog=Medicine_Distribution_System;Integrated Security=True";


        SqlConnection Con = new SqlConnection(connection);
        public Product()
        {
            InitializeComponent();
        }

        private void cmb_co_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            Con.Open();
            try
            {
                string Query = "insert into product (ProductID,ProductName,Munit,PurchaseRate,SaleRate,CompanyID) values(" + txt_productid.Text + ",'" + txt_productname.Text +"','" + txt_munit.Text+"',"+txt_prate.Text+","+txt_srate.Text+","+10+")";

                SqlCommand cmd = new SqlCommand(Query, Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Has been save", "MESSAGE");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Failed to add");
            }
            
            Con.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
           txt_productid.Text=Final_Project_Dsa.Main_Function.Main_Function.GetMaxId("Select ISNULL(MAX(ProductID),0) AS ID FROM PRODUCT").ToString();
        }
    }
}
