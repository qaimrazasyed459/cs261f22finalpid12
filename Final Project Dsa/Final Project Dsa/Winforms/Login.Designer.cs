﻿namespace Final_Project_Dsa
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btn_login = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Projectname = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel_guest = new System.Windows.Forms.Panel();
            this.check_Rider = new System.Windows.Forms.CheckBox();
            this.check_Employ = new System.Windows.Forms.CheckBox();
            this.check_cashier = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel_guest.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(18, 212);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 26);
            this.button1.TabIndex = 6;
            this.button1.Text = "As Guest";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.White;
            this.btn_login.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_login.Location = new System.Drawing.Point(206, 188);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(75, 28);
            this.btn_login.TabIndex = 5;
            this.btn_login.Text = "LogIn";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(15, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(40, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Email:";
            // 
            // lbl_Projectname
            // 
            this.lbl_Projectname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Projectname.AutoSize = true;
            this.lbl_Projectname.Font = new System.Drawing.Font("Sitka Small", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Projectname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_Projectname.Location = new System.Drawing.Point(58, 15);
            this.lbl_Projectname.Name = "lbl_Projectname";
            this.lbl_Projectname.Size = new System.Drawing.Size(195, 48);
            this.lbl_Projectname.TabIndex = 0;
            this.lbl_Projectname.Text = "Medicine Distribution\r\n      Management";
            this.lbl_Projectname.Click += new System.EventHandler(this.lbl_Projectname_Click);
            // 
            // txt_password
            // 
            this.txt_password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_password.Location = new System.Drawing.Point(99, 138);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(169, 20);
            this.txt_password.TabIndex = 2;
            // 
            // txt_email
            // 
            this.txt_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_email.Location = new System.Drawing.Point(99, 90);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(169, 20);
            this.txt_email.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.panel_guest);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.lbl_Projectname);
            this.panel1.Controls.Add(this.btn_login);
            this.panel1.Controls.Add(this.txt_email);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_password);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(223, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 295);
            this.panel1.TabIndex = 1;
            // 
            // panel_guest
            // 
            this.panel_guest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel_guest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.panel_guest.Controls.Add(this.check_cashier);
            this.panel_guest.Controls.Add(this.check_Employ);
            this.panel_guest.Controls.Add(this.check_Rider);
            this.panel_guest.Location = new System.Drawing.Point(19, 244);
            this.panel_guest.Name = "panel_guest";
            this.panel_guest.Size = new System.Drawing.Size(262, 37);
            this.panel_guest.TabIndex = 7;
            this.panel_guest.Visible = false;
            // 
            // check_Rider
            // 
            this.check_Rider.AutoSize = true;
            this.check_Rider.Font = new System.Drawing.Font("Sitka Small", 9.75F);
            this.check_Rider.Location = new System.Drawing.Point(11, 5);
            this.check_Rider.Name = "check_Rider";
            this.check_Rider.Size = new System.Drawing.Size(63, 23);
            this.check_Rider.TabIndex = 0;
            this.check_Rider.Text = "Rider";
            this.check_Rider.UseVisualStyleBackColor = true;
            this.check_Rider.CheckedChanged += new System.EventHandler(this.check_Rider_CheckedChanged);
            // 
            // check_Employ
            // 
            this.check_Employ.AutoSize = true;
            this.check_Employ.Font = new System.Drawing.Font("Sitka Small", 9.75F);
            this.check_Employ.Location = new System.Drawing.Point(80, 5);
            this.check_Employ.Name = "check_Employ";
            this.check_Employ.Size = new System.Drawing.Size(78, 23);
            this.check_Employ.TabIndex = 1;
            this.check_Employ.Text = "Employ";
            this.check_Employ.UseVisualStyleBackColor = true;
            this.check_Employ.CheckedChanged += new System.EventHandler(this.check_Employ_CheckedChanged);
            // 
            // check_cashier
            // 
            this.check_cashier.AutoSize = true;
            this.check_cashier.Font = new System.Drawing.Font("Sitka Small", 9.75F);
            this.check_cashier.Location = new System.Drawing.Point(171, 5);
            this.check_cashier.Name = "check_cashier";
            this.check_cashier.Size = new System.Drawing.Size(78, 23);
            this.check_cashier.TabIndex = 2;
            this.check_cashier.Text = "Cashier";
            this.check_cashier.UseVisualStyleBackColor = true;
            this.check_cashier.CheckedChanged += new System.EventHandler(this.check_cashier_CheckedChanged);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(714, 425);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(730, 464);
            this.Name = "Login";
            this.Text = "LogIn";
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_guest.ResumeLayout(false);
            this.panel_guest.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_Projectname;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel_guest;
        private System.Windows.Forms.CheckBox check_cashier;
        private System.Windows.Forms.CheckBox check_Employ;
        private System.Windows.Forms.CheckBox check_Rider;
    }
}

