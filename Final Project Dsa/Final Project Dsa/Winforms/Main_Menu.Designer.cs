﻿namespace Final_Project_Dsa
{
    partial class Main_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_main = new System.Windows.Forms.Panel();
            this.btn_contact = new System.Windows.Forms.Button();
            this.btn_service = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_mode = new System.Windows.Forms.Label();
            this.lbl_Projectname = new System.Windows.Forms.Label();
            this.slide_time = new System.Windows.Forms.Timer(this.components);
            this.btn_shopkeeper = new System.Windows.Forms.Button();
            this.btn_Rider = new System.Windows.Forms.Button();
            this.btn_Cashier = new System.Windows.Forms.Button();
            this.btn_Manager = new System.Windows.Forms.Button();
            this.mode_panel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Menu_pic = new System.Windows.Forms.PictureBox();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_home = new System.Windows.Forms.Button();
            this.panel_main.SuspendLayout();
            this.panel1.SuspendLayout();
            this.mode_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_main
            // 
            this.panel_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_main.Controls.Add(this.btn_exit);
            this.panel_main.Controls.Add(this.btn_home);
            this.panel_main.Controls.Add(this.btn_contact);
            this.panel_main.Controls.Add(this.btn_service);
            this.panel_main.Location = new System.Drawing.Point(0, -1);
            this.panel_main.MaximumSize = new System.Drawing.Size(183, 427);
            this.panel_main.MinimumSize = new System.Drawing.Size(60, 427);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(60, 427);
            this.panel_main.TabIndex = 0;
            // 
            // btn_contact
            // 
            this.btn_contact.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_contact.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_contact.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_contact.Location = new System.Drawing.Point(3, 235);
            this.btn_contact.MaximumSize = new System.Drawing.Size(259, 52);
            this.btn_contact.MinimumSize = new System.Drawing.Size(175, 39);
            this.btn_contact.Name = "btn_contact";
            this.btn_contact.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.btn_contact.Size = new System.Drawing.Size(175, 39);
            this.btn_contact.TabIndex = 4;
            this.btn_contact.Text = "Contact us";
            this.btn_contact.UseVisualStyleBackColor = true;
            // 
            // btn_service
            // 
            this.btn_service.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_service.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_service.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_service.Location = new System.Drawing.Point(3, 180);
            this.btn_service.MaximumSize = new System.Drawing.Size(259, 52);
            this.btn_service.MinimumSize = new System.Drawing.Size(175, 39);
            this.btn_service.Name = "btn_service";
            this.btn_service.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btn_service.Size = new System.Drawing.Size(175, 39);
            this.btn_service.TabIndex = 3;
            this.btn_service.Text = "Service";
            this.btn_service.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(80)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lbl_mode);
            this.panel1.Controls.Add(this.Menu_pic);
            this.panel1.Controls.Add(this.lbl_Projectname);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.MaximumSize = new System.Drawing.Size(1432, 107);
            this.panel1.MinimumSize = new System.Drawing.Size(712, 69);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(712, 69);
            this.panel1.TabIndex = 1;
            // 
            // lbl_mode
            // 
            this.lbl_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_mode.AutoSize = true;
            this.lbl_mode.BackColor = System.Drawing.Color.Transparent;
            this.lbl_mode.Font = new System.Drawing.Font("Sitka Small", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mode.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_mode.Location = new System.Drawing.Point(585, 39);
            this.lbl_mode.Name = "lbl_mode";
            this.lbl_mode.Size = new System.Drawing.Size(56, 24);
            this.lbl_mode.TabIndex = 3;
            this.lbl_mode.Text = "Mode";
            this.lbl_mode.Click += new System.EventHandler(this.lbl_mode_Click);
            // 
            // lbl_Projectname
            // 
            this.lbl_Projectname.AllowDrop = true;
            this.lbl_Projectname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Projectname.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Projectname.Font = new System.Drawing.Font("Sitka Small", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Projectname.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_Projectname.Location = new System.Drawing.Point(122, 22);
            this.lbl_Projectname.MaximumSize = new System.Drawing.Size(570, 68);
            this.lbl_Projectname.MinimumSize = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.Name = "lbl_Projectname";
            this.lbl_Projectname.Size = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.TabIndex = 1;
            this.lbl_Projectname.Text = "Medicine Distribution Management";
            this.lbl_Projectname.Click += new System.EventHandler(this.lbl_Projectname_Click);
            // 
            // slide_time
            // 
            this.slide_time.Tick += new System.EventHandler(this.Slide_Time_Tick);
            // 
            // btn_shopkeeper
            // 
            this.btn_shopkeeper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_shopkeeper.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_shopkeeper.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_shopkeeper.Location = new System.Drawing.Point(5, 112);
            this.btn_shopkeeper.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_shopkeeper.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_shopkeeper.Name = "btn_shopkeeper";
            this.btn_shopkeeper.Size = new System.Drawing.Size(123, 28);
            this.btn_shopkeeper.TabIndex = 6;
            this.btn_shopkeeper.Text = "Shopkeeper";
            this.btn_shopkeeper.UseVisualStyleBackColor = true;
            this.btn_shopkeeper.Click += new System.EventHandler(this.btn_shopkeeper_Click);
            // 
            // btn_Rider
            // 
            this.btn_Rider.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Rider.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Rider.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Rider.Location = new System.Drawing.Point(5, 80);
            this.btn_Rider.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Rider.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Rider.Name = "btn_Rider";
            this.btn_Rider.Size = new System.Drawing.Size(123, 28);
            this.btn_Rider.TabIndex = 5;
            this.btn_Rider.Text = "Rider";
            this.btn_Rider.UseVisualStyleBackColor = true;
            this.btn_Rider.Click += new System.EventHandler(this.btn_Rider_Click);
            // 
            // btn_Cashier
            // 
            this.btn_Cashier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cashier.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cashier.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Cashier.Location = new System.Drawing.Point(5, 46);
            this.btn_Cashier.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Cashier.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Cashier.Name = "btn_Cashier";
            this.btn_Cashier.Size = new System.Drawing.Size(123, 28);
            this.btn_Cashier.TabIndex = 4;
            this.btn_Cashier.Text = "Cashier";
            this.btn_Cashier.UseVisualStyleBackColor = true;
            this.btn_Cashier.Click += new System.EventHandler(this.btn_Cashier_Click);
            // 
            // btn_Manager
            // 
            this.btn_Manager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Manager.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Manager.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Manager.Location = new System.Drawing.Point(5, 12);
            this.btn_Manager.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Manager.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Manager.Name = "btn_Manager";
            this.btn_Manager.Size = new System.Drawing.Size(123, 28);
            this.btn_Manager.TabIndex = 3;
            this.btn_Manager.Text = "Manager";
            this.btn_Manager.UseVisualStyleBackColor = true;
            this.btn_Manager.Click += new System.EventHandler(this.btn_Manager_Click);
            // 
            // mode_panel
            // 
            this.mode_panel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.mode_panel.Controls.Add(this.btn_Manager);
            this.mode_panel.Controls.Add(this.btn_shopkeeper);
            this.mode_panel.Controls.Add(this.btn_Rider);
            this.mode_panel.Controls.Add(this.btn_Cashier);
            this.mode_panel.Location = new System.Drawing.Point(579, 74);
            this.mode_panel.MaximumSize = new System.Drawing.Size(209, 218);
            this.mode_panel.MinimumSize = new System.Drawing.Size(133, 143);
            this.mode_panel.Name = "mode_panel";
            this.mode_panel.Size = new System.Drawing.Size(133, 143);
            this.mode_panel.TabIndex = 7;
            this.mode_panel.Visible = false;
            this.mode_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.mode_panel_Paint);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Location = new System.Drawing.Point(66, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(643, 338);
            this.panel2.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Final_Project_Dsa.Properties.Resources.user1;
            this.pictureBox1.Location = new System.Drawing.Point(647, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Menu_pic
            // 
            this.Menu_pic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Menu_pic.Image = global::Final_Project_Dsa.Properties.Resources._160_1608249_menu_icon_png_circle_transparent_png_removebg_preview;
            this.Menu_pic.Location = new System.Drawing.Point(31, 16);
            this.Menu_pic.MaximumSize = new System.Drawing.Size(91, 70);
            this.Menu_pic.MinimumSize = new System.Drawing.Size(73, 50);
            this.Menu_pic.Name = "Menu_pic";
            this.Menu_pic.Size = new System.Drawing.Size(73, 50);
            this.Menu_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu_pic.TabIndex = 2;
            this.Menu_pic.TabStop = false;
            this.Menu_pic.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_exit.Image = global::Final_Project_Dsa.Properties.Resources.Exit_3;
            this.btn_exit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exit.Location = new System.Drawing.Point(3, 289);
            this.btn_exit.MaximumSize = new System.Drawing.Size(259, 52);
            this.btn_exit.MinimumSize = new System.Drawing.Size(175, 39);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_exit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_exit.Size = new System.Drawing.Size(175, 39);
            this.btn_exit.TabIndex = 5;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_home
            // 
            this.btn_home.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_home.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_home.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_home.Image = global::Final_Project_Dsa.Properties.Resources.home_7_line;
            this.btn_home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_home.Location = new System.Drawing.Point(3, 126);
            this.btn_home.MaximumSize = new System.Drawing.Size(259, 52);
            this.btn_home.MinimumSize = new System.Drawing.Size(175, 39);
            this.btn_home.Name = "btn_home";
            this.btn_home.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_home.Size = new System.Drawing.Size(175, 39);
            this.btn_home.TabIndex = 2;
            this.btn_home.Text = "Home";
            this.btn_home.UseVisualStyleBackColor = true;
            // 
            // Main_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(714, 425);
            this.Controls.Add(this.mode_panel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.panel2);
            this.MinimumSize = new System.Drawing.Size(730, 464);
            this.Name = "Main_Menu";
            this.Text = "Main_Menu";
            this.Load += new System.EventHandler(this.Main_Menu_Load);
            this.panel_main.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.mode_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Projectname;
        private System.Windows.Forms.PictureBox Menu_pic;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_home;
        private System.Windows.Forms.Button btn_contact;
        private System.Windows.Forms.Button btn_service;
        private System.Windows.Forms.Label lbl_mode;
        private System.Windows.Forms.Timer slide_time;
        private System.Windows.Forms.Button btn_shopkeeper;
        private System.Windows.Forms.Button btn_Rider;
        private System.Windows.Forms.Button btn_Cashier;
        private System.Windows.Forms.Button btn_Manager;
        private System.Windows.Forms.Panel mode_panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}