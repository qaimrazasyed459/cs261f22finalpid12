﻿namespace Final_Project_Dsa
{
    partial class Company
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_companyid = new System.Windows.Forms.Label();
            this.lbl_companyname = new System.Windows.Forms.Label();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_cname = new System.Windows.Forms.TextBox();
            this.btn_reset = new System.Windows.Forms.Button();
            this.lbl_description = new System.Windows.Forms.Label();
            this.txt_description = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_new = new System.Windows.Forms.Button();
            this.search = new System.Windows.Forms.TextBox();
            this.btn_del = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.grp_cfill = new System.Windows.Forms.GroupBox();
            this.grp_cview = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.grp_cfill.SuspendLayout();
            this.grp_cview.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_companyid
            // 
            this.lbl_companyid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_companyid.AutoSize = true;
            this.lbl_companyid.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyid.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_companyid.Location = new System.Drawing.Point(44, 38);
            this.lbl_companyid.Name = "lbl_companyid";
            this.lbl_companyid.Size = new System.Drawing.Size(90, 19);
            this.lbl_companyid.TabIndex = 18;
            this.lbl_companyid.Text = "CompanyID:";
            // 
            // lbl_companyname
            // 
            this.lbl_companyname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_companyname.AutoSize = true;
            this.lbl_companyname.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_companyname.Location = new System.Drawing.Point(22, 74);
            this.lbl_companyname.Name = "lbl_companyname";
            this.lbl_companyname.Size = new System.Drawing.Size(117, 19);
            this.lbl_companyname.TabIndex = 19;
            this.lbl_companyname.Text = "Company Name:";
            // 
            // txt_id
            // 
            this.txt_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_id.Location = new System.Drawing.Point(145, 39);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(67, 20);
            this.txt_id.TabIndex = 20;
            this.txt_id.TextChanged += new System.EventHandler(this.txt_id_TextChanged);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_save.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_save.Location = new System.Drawing.Point(113, 113);
            this.btn_save.MinimumSize = new System.Drawing.Size(59, 27);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(59, 27);
            this.btn_save.TabIndex = 21;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // txt_cname
            // 
            this.txt_cname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_cname.Location = new System.Drawing.Point(145, 75);
            this.txt_cname.Name = "txt_cname";
            this.txt_cname.Size = new System.Drawing.Size(145, 20);
            this.txt_cname.TabIndex = 22;
            // 
            // btn_reset
            // 
            this.btn_reset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_reset.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_reset.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_reset.Location = new System.Drawing.Point(250, 113);
            this.btn_reset.MinimumSize = new System.Drawing.Size(66, 27);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(66, 27);
            this.btn_reset.TabIndex = 24;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            // 
            // lbl_description
            // 
            this.lbl_description.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_description.AutoSize = true;
            this.lbl_description.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_description.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_description.Location = new System.Drawing.Point(338, 55);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(90, 19);
            this.lbl_description.TabIndex = 26;
            this.lbl_description.Text = "Description:";
            // 
            // txt_description
            // 
            this.txt_description.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_description.Location = new System.Drawing.Point(434, 38);
            this.txt_description.Multiline = true;
            this.txt_description.Name = "txt_description";
            this.txt_description.Size = new System.Drawing.Size(168, 53);
            this.txt_description.TabIndex = 27;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(121, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(456, 148);
            this.dataGridView1.TabIndex = 19;
            // 
            // btn_new
            // 
            this.btn_new.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_new.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_new.Location = new System.Drawing.Point(48, 113);
            this.btn_new.MinimumSize = new System.Drawing.Size(59, 27);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(59, 27);
            this.btn_new.TabIndex = 25;
            this.btn_new.Text = "New";
            this.btn_new.UseVisualStyleBackColor = true;
            // 
            // search
            // 
            this.search.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.search.Location = new System.Drawing.Point(224, 22);
            this.search.Multiline = true;
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(241, 23);
            this.search.TabIndex = 20;
            // 
            // btn_del
            // 
            this.btn_del.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_del.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_del.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_del.Location = new System.Drawing.Point(178, 113);
            this.btn_del.MinimumSize = new System.Drawing.Size(66, 27);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(66, 27);
            this.btn_del.TabIndex = 23;
            this.btn_del.Text = "Delete";
            this.btn_del.UseVisualStyleBackColor = true;
            // 
            // btn_search
            // 
            this.btn_search.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_search.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_search.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_search.Location = new System.Drawing.Point(471, 22);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(106, 27);
            this.btn_search.TabIndex = 21;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(109, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 23);
            this.label3.TabIndex = 22;
            this.label3.Text = "Company Name:";
            // 
            // grp_cfill
            // 
            this.grp_cfill.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_cfill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.grp_cfill.Controls.Add(this.lbl_companyid);
            this.grp_cfill.Controls.Add(this.lbl_companyname);
            this.grp_cfill.Controls.Add(this.btn_del);
            this.grp_cfill.Controls.Add(this.txt_description);
            this.grp_cfill.Controls.Add(this.txt_id);
            this.grp_cfill.Controls.Add(this.lbl_description);
            this.grp_cfill.Controls.Add(this.btn_save);
            this.grp_cfill.Controls.Add(this.btn_reset);
            this.grp_cfill.Controls.Add(this.btn_new);
            this.grp_cfill.Controls.Add(this.txt_cname);
            this.grp_cfill.Location = new System.Drawing.Point(12, 12);
            this.grp_cfill.MinimumSize = new System.Drawing.Size(674, 163);
            this.grp_cfill.Name = "grp_cfill";
            this.grp_cfill.Size = new System.Drawing.Size(675, 163);
            this.grp_cfill.TabIndex = 1;
            this.grp_cfill.TabStop = false;
            // 
            // grp_cview
            // 
            this.grp_cview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_cview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.grp_cview.Controls.Add(this.btn_search);
            this.grp_cview.Controls.Add(this.label3);
            this.grp_cview.Controls.Add(this.dataGridView1);
            this.grp_cview.Controls.Add(this.search);
            this.grp_cview.Location = new System.Drawing.Point(12, 181);
            this.grp_cview.Name = "grp_cview";
            this.grp_cview.Size = new System.Drawing.Size(675, 208);
            this.grp_cview.TabIndex = 2;
            this.grp_cview.TabStop = false;
            // 
            // Company
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(705, 411);
            this.Controls.Add(this.grp_cview);
            this.Controls.Add(this.grp_cfill);
            this.MinimumSize = new System.Drawing.Size(721, 450);
            this.Name = "Company";
            this.Text = "Company";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.grp_cfill.ResumeLayout(false);
            this.grp_cfill.PerformLayout();
            this.grp_cview.ResumeLayout(false);
            this.grp_cview.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_companyid;
        private System.Windows.Forms.Label lbl_companyname;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox txt_cname;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Label lbl_description;
        private System.Windows.Forms.TextBox txt_description;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.TextBox search;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grp_cfill;
        private System.Windows.Forms.GroupBox grp_cview;
    }
}