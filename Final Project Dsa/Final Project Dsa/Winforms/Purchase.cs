﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa
{
    public partial class Purchase : Form
    {
        
        public Purchase()
        {
            InitializeComponent();
        }

        private void txt_Description_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lbl_munit_Click(object sender, EventArgs e)
        {
           
        }

        private void Purchase_Load(object sender, EventArgs e)
        {
         
        }

        private void btn_max_Click(object sender, EventArgs e)
        {
            if (MaximizeBox)
            {
                maxsize();
            }
        }

        private void btn_party_Click(object sender, EventArgs e)
        {

            InitializeComponent();
            //if (MinimizeBox)
            //{

              //  minisize();

            //}
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {

        }

        private void Purchase_ResizeEnd(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                maxsize();
            }
        }


        private void minisize()
        {
            panel3.Size = new Size(689, 125);
            panel_pm.Size = new Size(689, 262);

            date1.Location = new Point(574, 10);
            date1.Size = new Size(121, 20);
            lbl_date.Size = new Size(46, 19);
            lbl_date.Location = new Point(506, 12);


            txt_billno.Location = new Point(574, 56);
            txt_billno.Size = new Size(121, 20);
            lbl_bill.Size = new Size(59, 19);
            lbl_bill.Location = new Point(493, 56);

            combo_pay.Location = new Point(574, 96);
            combo_pay.Size = new Size(121, 20);
            lbl_payment.Size = new Size(73, 19);
            lbl_payment.Location = new Point(479, 98);


            //Panel pm


            lbl_pid.Location = new Point(3, 11);
            lbl_pid.Size = new Size(87, 20);
            lbl_pid.Font = new Font("Sitka Small", 10);


            lbl_pro_id.Location = new Point(85, 11);
            lbl_pro_id.Size = new Size(170, 20);
            lbl_pro_id.Font = new Font("Sitka Small", 10);

            lbl_munit.Location = new Point(246, 11);
            lbl_munit.Size = new Size(100, 20);
            lbl_munit.Font = new Font("Sitka Small", 10);


            lbl_qty.Location = new Point(328, 11);
            lbl_qty.Size = new Size(94, 20);
            lbl_qty.Font = new Font("Sitka Small", 10);

            lbl_prate.Location = new Point(415, 11);
            lbl_prate.Size = new Size(97, 21);
            lbl_prate.Font = new Font("Sitka Small", 10);

            lbl_ptax.Location = new Point(506, 11);
            lbl_ptax.Size = new Size(98, 20);
            lbl_ptax.Font = new Font("Sitka Small", 10);

            lbl_amount.Location = new Point(596, 11);
            lbl_amount.Size = new Size(89, 20);
            lbl_amount.Font = new Font("Sitka Small", 10);



            txt_productid.Location = new Point(26, 35);
            txt_productid.Size = new Size(64, 20);

            txt_productname.Location = new Point(96, 35);
            txt_productname.Size = new Size(159, 20);

            txt_munit.Location = new Point(261, 35);
            txt_munit.Size = new Size(76, 20);

            txt_qty.Location = new Point(343, 35);
            txt_qty.Size = new Size(79, 20);

            txt_prate.Location = new Point(428, 35);
            txt_prate.Size = new Size(86, 20);

            txt_ptax.Location = new Point(520, 35);
            txt_ptax.Size = new Size(78, 20);

            txt_amount.Location = new Point(604, 34);
            txt_amount.Size = new Size(81, 20);

            ManualDG.Location = new Point(3, 60);
            ManualDG.Size = new Size(682, 156);

            btn_new.Location = new Point(3, 226);
            btn_new.Size = new Size(55, 29);

            btn_save.Location = new Point(64, 226);
            btn_save.Size = new Size(55, 29);

            btn_delete.Location = new Point(125, 226);
            btn_delete.Size = new Size(60, 29);

            btn_reset.Location = new Point(189, 226);
            btn_reset.Size = new Size(55, 29);

            btn_print.Location = new Point(250, 226);
            btn_print.Size = new Size(55, 29);

            lbl_total.Location = new Point(370, 232);
            txt_tqty.Location = new Point(428, 232);
            lbl_amou.Location = new Point(521, 233);
            txt_tamount.Location = new Point(594, 233);
        }

        private void maxsize()
        {
            panel3.Size = new Size(838, 355);

            panel_pm.Size = new Size(838, 146);



            date1.Location = new Point(703, 9);
            date1.Size = new Size(121, 20);
            lbl_date.Size = new Size(46, 19);
            lbl_date.Location = new Point(646, 9);


            txt_billno.Location = new Point(703, 55);
            txt_billno.Size = new Size(121, 20);
            lbl_bill.Size = new Size(59, 19);
            lbl_bill.Location = new Point(633, 53);

            combo_pay.Location = new Point(714, 93);
            combo_pay.Size = new Size(121, 20);
            lbl_payment.Size = new Size(73, 19);
            lbl_payment.Location = new Point(619, 95);



            lbl_pid.Location = new Point(34, 10);
            lbl_pid.Size = new Size(87, 21);
            lbl_pid.Font = new Font("Sitka Small", 11);


            lbl_pro_id.Location = new Point(118, 10);
            lbl_pro_id.Size = new Size(192, 21);
            lbl_pro_id.Font = new Font("Sitka Small", 11);

            lbl_munit.Location = new Point(306, 10);
            lbl_munit.Size = new Size(100, 21);
            lbl_munit.Font = new Font("Sitka Small", 11);


            lbl_qty.Location = new Point(403, 10);
            lbl_qty.Size = new Size(110, 21);
            lbl_qty.Font = new Font("Sitka Small", 11);

            lbl_prate.Location = new Point(506, 10);
            lbl_prate.Size = new Size(97, 21);
            lbl_prate.Font = new Font("Sitka Small", 11);

            lbl_ptax.Location = new Point(598, 10);
            lbl_ptax.Size = new Size(109, 21);
            lbl_ptax.Font = new Font("Sitka Small", 11);

            lbl_amount.Location = new Point(704, 10);
            lbl_amount.Size = new Size(108, 21);
            lbl_amount.Font = new Font("Sitka Small", 11);



            txt_productid.Location = new Point(38, 43);
            txt_productid.Size = new Size(77, 20);

            txt_productname.Location = new Point(121, 43);
            txt_productname.Size = new Size(189, 20);

            txt_munit.Location = new Point(316, 43);
            txt_munit.Size = new Size(97, 20);

            txt_qty.Location = new Point(419, 43);
            txt_qty.Size = new Size(97, 20);

            txt_prate.Location = new Point(522, 43);
            txt_prate.Size = new Size(86, 20);

            txt_ptax.Location = new Point(614, 43);
            txt_ptax.Size = new Size(92, 20);

            txt_amount.Location = new Point(713, 43);
            txt_amount.Size = new Size(102, 20);

            ManualDG.Location = new Point(6, 69);
            ManualDG.Size = new Size(819, 180);

            btn_new.Location = new Point(7, 260);
            btn_new.Size = new Size(70, 29);

            btn_save.Location = new Point(81, 260);
            btn_save.Size = new Size(70, 29);

            btn_delete.Location = new Point(157, 260);
            btn_delete.Size = new Size(70, 29);

            btn_reset.Location = new Point(233, 260);
            btn_reset.Size = new Size(70, 29);

            btn_print.Location = new Point(309, 260);
            btn_print.Size = new Size(70, 29);

            lbl_total.Location = new Point(511, 264);
            txt_tqty.Location = new Point(569, 264);
            lbl_amou.Location = new Point(662, 265);
            txt_tamount.Location = new Point(735, 265);
        }
    }
}
