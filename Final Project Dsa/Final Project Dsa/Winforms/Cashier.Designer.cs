﻿namespace Final_Project_Dsa
{
    partial class Cashier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_main = new System.Windows.Forms.Panel();
            this.btn_product = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel_cashier = new System.Windows.Forms.Panel();
            this.lbl_mode = new System.Windows.Forms.Label();
            this.lbl_Projectname = new System.Windows.Forms.Label();
            this.mode_panel = new System.Windows.Forms.Panel();
            this.btn_Manager = new System.Windows.Forms.Button();
            this.btn_shopkeeper = new System.Windows.Forms.Button();
            this.btn_Rider = new System.Windows.Forms.Button();
            this.btn_Cashier = new System.Windows.Forms.Button();
            this.slide_time = new System.Windows.Forms.Timer(this.components);
            this.btn_panel = new System.Windows.Forms.Panel();
            this.Menu_pic = new System.Windows.Forms.PictureBox();
            this.btn_pproduct = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_report = new System.Windows.Forms.Button();
            this.btn_sale = new System.Windows.Forms.Button();
            this.btn_purchase = new System.Windows.Forms.Button();
            this.btn_company = new System.Windows.Forms.Button();
            this.btn_home = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_main.SuspendLayout();
            this.panel_cashier.SuspendLayout();
            this.mode_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_main
            // 
            this.panel_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_main.Controls.Add(this.btn_pproduct);
            this.panel_main.Controls.Add(this.btn_exit);
            this.panel_main.Controls.Add(this.btn_report);
            this.panel_main.Controls.Add(this.btn_sale);
            this.panel_main.Controls.Add(this.btn_purchase);
            this.panel_main.Controls.Add(this.btn_company);
            this.panel_main.Controls.Add(this.btn_home);
            this.panel_main.Location = new System.Drawing.Point(1, 1);
            this.panel_main.MaximumSize = new System.Drawing.Size(184, 427);
            this.panel_main.MinimumSize = new System.Drawing.Size(60, 700);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(184, 700);
            this.panel_main.TabIndex = 1;
            // 
            // btn_product
            // 
            this.btn_product.Location = new System.Drawing.Point(0, 0);
            this.btn_product.Name = "btn_product";
            this.btn_product.Size = new System.Drawing.Size(75, 23);
            this.btn_product.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(3, 156);
            this.button1.MaximumSize = new System.Drawing.Size(177, 35);
            this.button1.MinimumSize = new System.Drawing.Size(177, 35);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(177, 35);
            this.button1.TabIndex = 17;
            this.button1.Text = "Company";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel_cashier
            // 
            this.panel_cashier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_cashier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(80)))));
            this.panel_cashier.Controls.Add(this.pictureBox1);
            this.panel_cashier.Controls.Add(this.lbl_mode);
            this.panel_cashier.Controls.Add(this.Menu_pic);
            this.panel_cashier.Controls.Add(this.lbl_Projectname);
            this.panel_cashier.Location = new System.Drawing.Point(1, 1);
            this.panel_cashier.MaximumSize = new System.Drawing.Size(1432, 107);
            this.panel_cashier.MinimumSize = new System.Drawing.Size(722, 69);
            this.panel_cashier.Name = "panel_cashier";
            this.panel_cashier.Size = new System.Drawing.Size(768, 69);
            this.panel_cashier.TabIndex = 2;
            // 
            // lbl_mode
            // 
            this.lbl_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_mode.AutoSize = true;
            this.lbl_mode.BackColor = System.Drawing.Color.Transparent;
            this.lbl_mode.Font = new System.Drawing.Font("Sitka Small", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mode.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_mode.Location = new System.Drawing.Point(636, 39);
            this.lbl_mode.Name = "lbl_mode";
            this.lbl_mode.Size = new System.Drawing.Size(56, 24);
            this.lbl_mode.TabIndex = 3;
            this.lbl_mode.Text = "Mode";
            this.lbl_mode.Click += new System.EventHandler(this.lbl_mode_Click);
            // 
            // lbl_Projectname
            // 
            this.lbl_Projectname.AllowDrop = true;
            this.lbl_Projectname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Projectname.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Projectname.Font = new System.Drawing.Font("Sitka Small", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Projectname.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_Projectname.Location = new System.Drawing.Point(169, 22);
            this.lbl_Projectname.MaximumSize = new System.Drawing.Size(570, 68);
            this.lbl_Projectname.MinimumSize = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.Name = "lbl_Projectname";
            this.lbl_Projectname.Size = new System.Drawing.Size(411, 41);
            this.lbl_Projectname.TabIndex = 1;
            this.lbl_Projectname.Text = "Medicine Distribution Management";
            // 
            // mode_panel
            // 
            this.mode_panel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.mode_panel.Controls.Add(this.btn_Manager);
            this.mode_panel.Controls.Add(this.btn_shopkeeper);
            this.mode_panel.Controls.Add(this.btn_Rider);
            this.mode_panel.Controls.Add(this.btn_Cashier);
            this.mode_panel.Location = new System.Drawing.Point(636, 76);
            this.mode_panel.MaximumSize = new System.Drawing.Size(209, 218);
            this.mode_panel.MinimumSize = new System.Drawing.Size(133, 143);
            this.mode_panel.Name = "mode_panel";
            this.mode_panel.Size = new System.Drawing.Size(133, 143);
            this.mode_panel.TabIndex = 8;
            this.mode_panel.Visible = false;
            // 
            // btn_Manager
            // 
            this.btn_Manager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Manager.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Manager.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Manager.Location = new System.Drawing.Point(5, 12);
            this.btn_Manager.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Manager.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Manager.Name = "btn_Manager";
            this.btn_Manager.Size = new System.Drawing.Size(123, 28);
            this.btn_Manager.TabIndex = 3;
            this.btn_Manager.Text = "Manager";
            this.btn_Manager.UseVisualStyleBackColor = true;
            this.btn_Manager.Click += new System.EventHandler(this.btn_Manager_Click);
            // 
            // btn_shopkeeper
            // 
            this.btn_shopkeeper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_shopkeeper.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_shopkeeper.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_shopkeeper.Location = new System.Drawing.Point(5, 112);
            this.btn_shopkeeper.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_shopkeeper.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_shopkeeper.Name = "btn_shopkeeper";
            this.btn_shopkeeper.Size = new System.Drawing.Size(123, 28);
            this.btn_shopkeeper.TabIndex = 6;
            this.btn_shopkeeper.Text = "Shopkeeper";
            this.btn_shopkeeper.UseVisualStyleBackColor = true;
            this.btn_shopkeeper.Click += new System.EventHandler(this.btn_shopkeeper_Click);
            // 
            // btn_Rider
            // 
            this.btn_Rider.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Rider.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Rider.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Rider.Location = new System.Drawing.Point(5, 80);
            this.btn_Rider.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Rider.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Rider.Name = "btn_Rider";
            this.btn_Rider.Size = new System.Drawing.Size(123, 28);
            this.btn_Rider.TabIndex = 5;
            this.btn_Rider.Text = "Rider";
            this.btn_Rider.UseVisualStyleBackColor = true;
            this.btn_Rider.Click += new System.EventHandler(this.btn_Rider_Click);
            // 
            // btn_Cashier
            // 
            this.btn_Cashier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cashier.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cashier.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Cashier.Location = new System.Drawing.Point(5, 46);
            this.btn_Cashier.MaximumSize = new System.Drawing.Size(198, 45);
            this.btn_Cashier.MinimumSize = new System.Drawing.Size(123, 28);
            this.btn_Cashier.Name = "btn_Cashier";
            this.btn_Cashier.Size = new System.Drawing.Size(123, 28);
            this.btn_Cashier.TabIndex = 4;
            this.btn_Cashier.Text = "Cashier";
            this.btn_Cashier.UseVisualStyleBackColor = true;
            this.btn_Cashier.Click += new System.EventHandler(this.btn_Cashier_Click);
            // 
            // slide_time
            // 
            this.slide_time.Tick += new System.EventHandler(this.slide_time_Tick);
            // 
            // btn_panel
            // 
            this.btn_panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_panel.BackColor = System.Drawing.Color.White;
            this.btn_panel.Location = new System.Drawing.Point(67, 76);
            this.btn_panel.Name = "btn_panel";
            this.btn_panel.Size = new System.Drawing.Size(692, 396);
            this.btn_panel.TabIndex = 9;
            this.btn_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.btn_panel_Paint);
            // 
            // Menu_pic
            // 
            this.Menu_pic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Menu_pic.Image = global::Final_Project_Dsa.Properties.Resources._160_1608249_menu_icon_png_circle_transparent_png_removebg_preview;
            this.Menu_pic.Location = new System.Drawing.Point(31, 16);
            this.Menu_pic.MaximumSize = new System.Drawing.Size(91, 70);
            this.Menu_pic.MinimumSize = new System.Drawing.Size(73, 50);
            this.Menu_pic.Name = "Menu_pic";
            this.Menu_pic.Size = new System.Drawing.Size(91, 50);
            this.Menu_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu_pic.TabIndex = 2;
            this.Menu_pic.TabStop = false;
            this.Menu_pic.Click += new System.EventHandler(this.Menu_pic_Click);
            // 
            // btn_pproduct
            // 
            this.btn_pproduct.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_pproduct.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_pproduct.Image = global::Final_Project_Dsa.Properties.Resources.product;
            this.btn_pproduct.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pproduct.Location = new System.Drawing.Point(4, 160);
            this.btn_pproduct.Name = "btn_pproduct";
            this.btn_pproduct.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_pproduct.Size = new System.Drawing.Size(177, 32);
            this.btn_pproduct.TabIndex = 0;
            this.btn_pproduct.Text = "Product";
            this.btn_pproduct.UseVisualStyleBackColor = true;
            this.btn_pproduct.Click += new System.EventHandler(this.btn_pproduct_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_exit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_exit.Image = global::Final_Project_Dsa.Properties.Resources.Exit_3;
            this.btn_exit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exit.Location = new System.Drawing.Point(3, 382);
            this.btn_exit.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_exit.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_exit.Size = new System.Drawing.Size(177, 35);
            this.btn_exit.TabIndex = 15;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            // 
            // btn_report
            // 
            this.btn_report.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_report.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_report.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_report.Image = global::Final_Project_Dsa.Properties.Resources.file_chart_line;
            this.btn_report.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_report.Location = new System.Drawing.Point(3, 337);
            this.btn_report.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_report.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_report.Name = "btn_report";
            this.btn_report.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_report.Size = new System.Drawing.Size(177, 35);
            this.btn_report.TabIndex = 14;
            this.btn_report.Text = "Report";
            this.btn_report.UseVisualStyleBackColor = true;
            this.btn_report.Click += new System.EventHandler(this.btn_report_Click);
            // 
            // btn_sale
            // 
            this.btn_sale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_sale.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_sale.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_sale.Image = global::Final_Project_Dsa.Properties.Resources.sale;
            this.btn_sale.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_sale.Location = new System.Drawing.Point(3, 292);
            this.btn_sale.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_sale.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_sale.Name = "btn_sale";
            this.btn_sale.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_sale.Size = new System.Drawing.Size(177, 35);
            this.btn_sale.TabIndex = 13;
            this.btn_sale.Text = "Sale";
            this.btn_sale.UseVisualStyleBackColor = true;
            this.btn_sale.Click += new System.EventHandler(this.btn_sale_Click);
            // 
            // btn_purchase
            // 
            this.btn_purchase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_purchase.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_purchase.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_purchase.Image = global::Final_Project_Dsa.Properties.Resources.shopping_cart_fill;
            this.btn_purchase.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_purchase.Location = new System.Drawing.Point(3, 247);
            this.btn_purchase.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_purchase.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_purchase.Name = "btn_purchase";
            this.btn_purchase.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_purchase.Size = new System.Drawing.Size(177, 35);
            this.btn_purchase.TabIndex = 12;
            this.btn_purchase.Text = "Purchase";
            this.btn_purchase.UseVisualStyleBackColor = true;
            this.btn_purchase.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_company
            // 
            this.btn_company.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_company.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_company.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_company.Image = global::Final_Project_Dsa.Properties.Resources.building_4_line;
            this.btn_company.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_company.Location = new System.Drawing.Point(3, 202);
            this.btn_company.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_company.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_company.Name = "btn_company";
            this.btn_company.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_company.Size = new System.Drawing.Size(177, 35);
            this.btn_company.TabIndex = 11;
            this.btn_company.Text = "Company";
            this.btn_company.UseVisualStyleBackColor = true;
            this.btn_company.Click += new System.EventHandler(this.btn_company_Click);
            // 
            // btn_home
            // 
            this.btn_home.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_home.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_home.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_home.Image = global::Final_Project_Dsa.Properties.Resources.home_7_line;
            this.btn_home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_home.Location = new System.Drawing.Point(3, 115);
            this.btn_home.MaximumSize = new System.Drawing.Size(177, 35);
            this.btn_home.MinimumSize = new System.Drawing.Size(177, 35);
            this.btn_home.Name = "btn_home";
            this.btn_home.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_home.Size = new System.Drawing.Size(177, 35);
            this.btn_home.TabIndex = 9;
            this.btn_home.Text = "Home";
            this.btn_home.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Final_Project_Dsa.Properties.Resources.user1;
            this.pictureBox1.Location = new System.Drawing.Point(698, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Cashier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(770, 484);
            this.Controls.Add(this.mode_panel);
            this.Controls.Add(this.panel_cashier);
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.btn_panel);
            this.MinimumSize = new System.Drawing.Size(786, 523);
            this.Name = "Cashier";
            this.Text = "Cashier";
            this.Load += new System.EventHandler(this.Cashier_Load);
            this.panel_main.ResumeLayout(false);
            this.panel_cashier.ResumeLayout(false);
            this.panel_cashier.PerformLayout();
            this.mode_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Menu_pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Panel panel_cashier;
        private System.Windows.Forms.Label lbl_mode;
        private System.Windows.Forms.PictureBox Menu_pic;
        private System.Windows.Forms.Label lbl_Projectname;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_report;
        private System.Windows.Forms.Button btn_sale;
        private System.Windows.Forms.Button btn_purchase;
        private System.Windows.Forms.Button btn_company;
        private System.Windows.Forms.Button btn_product;
        private System.Windows.Forms.Button btn_home;
        private System.Windows.Forms.Panel mode_panel;
        private System.Windows.Forms.Button btn_Manager;
        private System.Windows.Forms.Button btn_shopkeeper;
        private System.Windows.Forms.Button btn_Rider;
        private System.Windows.Forms.Button btn_Cashier;
        private System.Windows.Forms.Timer slide_time;
        private System.Windows.Forms.Panel btn_panel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_pproduct;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}