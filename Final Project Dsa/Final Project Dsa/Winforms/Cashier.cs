﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa
{
    public partial class Cashier : Form
    {
        bool slidebarexpand;
        public Cashier()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            btn_panel.Controls.Clear();
            Form P = new Purchase();

            P.Size = btn_panel.Size;
            P.TopLevel = false;
            P.Parent = btn_panel;
            P.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            //pictureBox2.Visible = false;
            P.Show();
        }

        private void Cashier_Load(object sender, EventArgs e)
        {
            if (MaximizeBox) {
            //    panel_main.MinimumSize = new Size(184, 427);
            //  panel_main.MaximumSize = new Size(265, 783);
                btn_panel.Size = new Size(879, 450);
            }
        }

        private void lbl_mode_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= true;
        }

        private void btn_Manager_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void btn_Cashier_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void btn_Rider_Click(object sender, EventArgs e)
        {
            mode_panel .Visible= false; 
        }

        private void btn_shopkeeper_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void Menu_pic_Click(object sender, EventArgs e)
        {
            slide_time.Start();
        }

        private void slide_time_Tick(object sender, EventArgs e)
        {
            if (slidebarexpand)
            {
                panel_main.Width -= 10;
                if (panel_main.Width == panel_main.MinimumSize.Width)
                {
                    slidebarexpand = false;
                    slide_time.Stop();
                }
            }
            else
            {
                panel_main.Width += 10;
                if (panel_main.Width == panel_main.MaximumSize.Width)
                {
                    slidebarexpand = true;
                    slide_time.Stop();
                }
            }
        }

        private void btn_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_report_Click(object sender, EventArgs e)
        {
            btn_panel.Controls.Clear();
            Form R = new Report();

            R.Size = btn_panel.Size;
            R.TopLevel = false;
            R.Parent = btn_panel;
            R.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            //pictureBox2.Visible = false;
            R.Show();
        }

        private void btn_company_Click(object sender, EventArgs e)
        {
            btn_panel.Controls.Clear();
            Form C = new Company();
            C.Size = btn_panel.Size;
            C.TopLevel = false;
           
            C.Parent = btn_panel;
            C.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            //pictureBox2.Visible = false;
            C.Show();
        }

       
         private void btn_pproduct_Click(object sender, EventArgs e)
        {

            btn_panel.Controls.Clear();
           Form P=new Winforms.Product();
            P.Size = btn_panel.Size;
            P.TopLevel = false;

            P.Parent = btn_panel;
            P.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            //pictureBox2.Visible = false;
            P.Show();
        }

        private void btn_sale_Click(object sender, EventArgs e)
        {
            btn_panel.Controls.Clear();
            Form S = new Sale();
            S.Size=btn_panel.Size;
            S.TopLevel = false;
            S.Parent = btn_panel;
            S.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            S.Show();
            S.Show();
        }
    }



 
    
}
