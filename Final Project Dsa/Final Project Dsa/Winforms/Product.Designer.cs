﻿namespace Final_Project_Dsa.Winforms
{
    partial class Product
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel_pfill = new System.Windows.Forms.Panel();
            this.btn_del = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.cmb_co = new System.Windows.Forms.ComboBox();
            this.txt_srate = new System.Windows.Forms.TextBox();
            this.lbl_company = new System.Windows.Forms.Label();
            this.lbl_munit = new System.Windows.Forms.Label();
            this.lbl_srate = new System.Windows.Forms.Label();
            this.lbl_prate = new System.Windows.Forms.Label();
            this.lbl_proname = new System.Windows.Forms.Label();
            this.lbl_proid = new System.Windows.Forms.Label();
            this.txt_munit = new System.Windows.Forms.TextBox();
            this.txt_prate = new System.Windows.Forms.TextBox();
            this.txt_productname = new System.Windows.Forms.TextBox();
            this.txt_productid = new System.Windows.Forms.TextBox();
            this.panel_pview = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel_pfill.SuspendLayout();
            this.panel_pview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel_pfill, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel_pview, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.94118F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.05882F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(714, 411);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel_pfill
            // 
            this.panel_pfill.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_pfill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_pfill.Controls.Add(this.btn_del);
            this.panel_pfill.Controls.Add(this.btn_save);
            this.panel_pfill.Controls.Add(this.btn_reset);
            this.panel_pfill.Controls.Add(this.btn_new);
            this.panel_pfill.Controls.Add(this.cmb_co);
            this.panel_pfill.Controls.Add(this.txt_srate);
            this.panel_pfill.Controls.Add(this.lbl_company);
            this.panel_pfill.Controls.Add(this.lbl_munit);
            this.panel_pfill.Controls.Add(this.lbl_srate);
            this.panel_pfill.Controls.Add(this.lbl_prate);
            this.panel_pfill.Controls.Add(this.lbl_proname);
            this.panel_pfill.Controls.Add(this.lbl_proid);
            this.panel_pfill.Controls.Add(this.txt_munit);
            this.panel_pfill.Controls.Add(this.txt_prate);
            this.panel_pfill.Controls.Add(this.txt_productname);
            this.panel_pfill.Controls.Add(this.txt_productid);
            this.panel_pfill.Location = new System.Drawing.Point(10, 10);
            this.panel_pfill.Margin = new System.Windows.Forms.Padding(10);
            this.panel_pfill.Name = "panel_pfill";
            this.panel_pfill.Size = new System.Drawing.Size(694, 181);
            this.panel_pfill.TabIndex = 0;
            // 
            // btn_del
            // 
            this.btn_del.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_del.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_del.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_del.Location = new System.Drawing.Point(267, 138);
            this.btn_del.MinimumSize = new System.Drawing.Size(66, 27);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(66, 27);
            this.btn_del.TabIndex = 38;
            this.btn_del.Text = "Delete";
            this.btn_del.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_save.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_save.Location = new System.Drawing.Point(202, 138);
            this.btn_save.MinimumSize = new System.Drawing.Size(59, 27);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(59, 27);
            this.btn_save.TabIndex = 37;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_reset
            // 
            this.btn_reset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_reset.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_reset.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_reset.Location = new System.Drawing.Point(339, 138);
            this.btn_reset.MinimumSize = new System.Drawing.Size(66, 27);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(66, 27);
            this.btn_reset.TabIndex = 39;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            // 
            // btn_new
            // 
            this.btn_new.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_new.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_new.Location = new System.Drawing.Point(137, 138);
            this.btn_new.MinimumSize = new System.Drawing.Size(59, 27);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(59, 27);
            this.btn_new.TabIndex = 40;
            this.btn_new.Text = "New";
            this.btn_new.UseVisualStyleBackColor = true;
            // 
            // cmb_co
            // 
            this.cmb_co.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmb_co.DisplayMember = "CompanyName";
            this.cmb_co.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_co.FormattingEnabled = true;
            this.cmb_co.Location = new System.Drawing.Point(405, 58);
            this.cmb_co.Name = "cmb_co";
            this.cmb_co.Size = new System.Drawing.Size(137, 21);
            this.cmb_co.TabIndex = 36;
            this.cmb_co.ValueMember = "CompanyID";
            this.cmb_co.SelectedIndexChanged += new System.EventHandler(this.cmb_co_SelectedIndexChanged);
            // 
            // txt_srate
            // 
            this.txt_srate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_srate.Location = new System.Drawing.Point(137, 92);
            this.txt_srate.Name = "txt_srate";
            this.txt_srate.Size = new System.Drawing.Size(100, 20);
            this.txt_srate.TabIndex = 34;
            this.txt_srate.Text = "0";
            this.txt_srate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_company
            // 
            this.lbl_company.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_company.AutoSize = true;
            this.lbl_company.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_company.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_company.Location = new System.Drawing.Point(323, 59);
            this.lbl_company.Name = "lbl_company";
            this.lbl_company.Size = new System.Drawing.Size(71, 19);
            this.lbl_company.TabIndex = 33;
            this.lbl_company.Text = "Company:";
            // 
            // lbl_munit
            // 
            this.lbl_munit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_munit.AutoSize = true;
            this.lbl_munit.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_munit.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_munit.Location = new System.Drawing.Point(343, 97);
            this.lbl_munit.Name = "lbl_munit";
            this.lbl_munit.Size = new System.Drawing.Size(55, 19);
            this.lbl_munit.TabIndex = 31;
            this.lbl_munit.Text = "M unit:";
            // 
            // lbl_srate
            // 
            this.lbl_srate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_srate.AutoSize = true;
            this.lbl_srate.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_srate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_srate.Location = new System.Drawing.Point(65, 92);
            this.lbl_srate.Name = "lbl_srate";
            this.lbl_srate.Size = new System.Drawing.Size(71, 19);
            this.lbl_srate.TabIndex = 30;
            this.lbl_srate.Text = "Sale Rate:";
            // 
            // lbl_prate
            // 
            this.lbl_prate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_prate.AutoSize = true;
            this.lbl_prate.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_prate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_prate.Location = new System.Drawing.Point(34, 60);
            this.lbl_prate.Name = "lbl_prate";
            this.lbl_prate.Size = new System.Drawing.Size(102, 19);
            this.lbl_prate.TabIndex = 29;
            this.lbl_prate.Text = "Purchase Rate:";
            // 
            // lbl_proname
            // 
            this.lbl_proname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_proname.AutoSize = true;
            this.lbl_proname.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_proname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_proname.Location = new System.Drawing.Point(296, 25);
            this.lbl_proname.Name = "lbl_proname";
            this.lbl_proname.Size = new System.Drawing.Size(98, 19);
            this.lbl_proname.TabIndex = 28;
            this.lbl_proname.Text = "ProductName:";
            // 
            // lbl_proid
            // 
            this.lbl_proid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_proid.AutoSize = true;
            this.lbl_proid.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.lbl_proid.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_proid.Location = new System.Drawing.Point(58, 24);
            this.lbl_proid.Name = "lbl_proid";
            this.lbl_proid.Size = new System.Drawing.Size(76, 19);
            this.lbl_proid.TabIndex = 27;
            this.lbl_proid.Text = "ProductID:";
            // 
            // txt_munit
            // 
            this.txt_munit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_munit.Location = new System.Drawing.Point(405, 96);
            this.txt_munit.Name = "txt_munit";
            this.txt_munit.Size = new System.Drawing.Size(100, 20);
            this.txt_munit.TabIndex = 26;
            // 
            // txt_prate
            // 
            this.txt_prate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_prate.Location = new System.Drawing.Point(137, 59);
            this.txt_prate.Name = "txt_prate";
            this.txt_prate.Size = new System.Drawing.Size(100, 20);
            this.txt_prate.TabIndex = 25;
            this.txt_prate.Text = "0";
            this.txt_prate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_productname
            // 
            this.txt_productname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_productname.Location = new System.Drawing.Point(405, 26);
            this.txt_productname.Name = "txt_productname";
            this.txt_productname.Size = new System.Drawing.Size(207, 20);
            this.txt_productname.TabIndex = 24;
            // 
            // txt_productid
            // 
            this.txt_productid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_productid.Location = new System.Drawing.Point(137, 24);
            this.txt_productid.Name = "txt_productid";
            this.txt_productid.Size = new System.Drawing.Size(75, 20);
            this.txt_productid.TabIndex = 23;
            this.txt_productid.Text = "0";
            this.txt_productid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel_pview
            // 
            this.panel_pview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_pview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_pview.Controls.Add(this.label1);
            this.panel_pview.Controls.Add(this.button1);
            this.panel_pview.Controls.Add(this.txt_search);
            this.panel_pview.Controls.Add(this.dataGridView1);
            this.panel_pview.Location = new System.Drawing.Point(10, 211);
            this.panel_pview.Margin = new System.Windows.Forms.Padding(10);
            this.panel_pview.Name = "panel_pview";
            this.panel_pview.Size = new System.Drawing.Size(694, 190);
            this.panel_pview.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sitka Text", 9.749999F);
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(99, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 19);
            this.label1.TabIndex = 41;
            this.label1.Text = "Product Name";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(445, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 28);
            this.button1.TabIndex = 22;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txt_search
            // 
            this.txt_search.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_search.Location = new System.Drawing.Point(202, 14);
            this.txt_search.Multiline = true;
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(221, 23);
            this.txt_search.TabIndex = 21;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CategoryName,
            this.CompanyName});
            this.dataGridView1.Location = new System.Drawing.Point(110, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(430, 120);
            this.dataGridView1.TabIndex = 20;
            // 
            // CategoryName
            // 
            this.CategoryName.DataPropertyName = "CategoryName";
            this.CategoryName.HeaderText = "CategoryName";
            this.CategoryName.Name = "CategoryName";
            this.CategoryName.ReadOnly = true;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "CompanyName";
            this.CompanyName.HeaderText = "CompanyName";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.ReadOnly = true;
            // 
            // Product
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 411);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(730, 442);
            this.Name = "Product";
            this.Text = "Product";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel_pfill.ResumeLayout(false);
            this.panel_pfill.PerformLayout();
            this.panel_pview.ResumeLayout(false);
            this.panel_pview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel_pfill;
        private System.Windows.Forms.Panel panel_pview;
        private System.Windows.Forms.ComboBox cmb_co;
        private System.Windows.Forms.TextBox txt_srate;
        private System.Windows.Forms.Label lbl_company;
        private System.Windows.Forms.Label lbl_munit;
        private System.Windows.Forms.Label lbl_srate;
        private System.Windows.Forms.Label lbl_prate;
        private System.Windows.Forms.Label lbl_proname;
        private System.Windows.Forms.Label lbl_proid;
        private System.Windows.Forms.TextBox txt_munit;
        private System.Windows.Forms.TextBox txt_prate;
        private System.Windows.Forms.TextBox txt_productname;
        private System.Windows.Forms.TextBox txt_productid;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.Label label1;
    }
}