﻿namespace Final_Project_Dsa
{
    partial class Purchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_description = new System.Windows.Forms.Label();
            this.txt_Description = new System.Windows.Forms.TextBox();
            this.btn_party = new System.Windows.Forms.Button();
            this.txt_party = new System.Windows.Forms.TextBox();
            this.txt_billno = new System.Windows.Forms.TextBox();
            this.lbl_bill = new System.Windows.Forms.Label();
            this.txt_partyid = new System.Windows.Forms.TextBox();
            this.txt_invno = new System.Windows.Forms.TextBox();
            this.lbl_invoice = new System.Windows.Forms.Label();
            this.date1 = new System.Windows.Forms.DateTimePicker();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_pname = new System.Windows.Forms.Label();
            this.lbl_partyid = new System.Windows.Forms.Label();
            this.lbl_payment = new System.Windows.Forms.Label();
            this.combo_pay = new System.Windows.Forms.ComboBox();
            this.panel_pm = new System.Windows.Forms.Panel();
            this.ManualDG = new System.Windows.Forms.DataGridView();
            this.ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.am = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_productname = new System.Windows.Forms.TextBox();
            this.lbl_pid = new System.Windows.Forms.Label();
            this.lbl_pro_id = new System.Windows.Forms.Label();
            this.lbl_munit = new System.Windows.Forms.Label();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_qty = new System.Windows.Forms.Label();
            this.lbl_prate = new System.Windows.Forms.Label();
            this.txt_productid = new System.Windows.Forms.TextBox();
            this.lbl_amount = new System.Windows.Forms.Label();
            this.txt_munit = new System.Windows.Forms.TextBox();
            this.txt_qty = new System.Windows.Forms.TextBox();
            this.txt_prate = new System.Windows.Forms.TextBox();
            this.txt_tqty = new System.Windows.Forms.TextBox();
            this.txt_amount = new System.Windows.Forms.TextBox();
            this.txt_tamount = new System.Windows.Forms.TextBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_max = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.lbl_amou = new System.Windows.Forms.Label();
            this.btn_print = new System.Windows.Forms.Button();
            this.lbl_ptax = new System.Windows.Forms.Label();
            this.txt_ptax = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel_pm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ManualDG)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_description
            // 
            this.lbl_description.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_description.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_description.Location = new System.Drawing.Point(12, 82);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(91, 21);
            this.lbl_description.TabIndex = 21;
            this.lbl_description.Text = "Description:";
            // 
            // txt_Description
            // 
            this.txt_Description.Location = new System.Drawing.Point(112, 82);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.Size = new System.Drawing.Size(261, 32);
            this.txt_Description.TabIndex = 20;
            this.txt_Description.TextChanged += new System.EventHandler(this.txt_Description_TextChanged);
            // 
            // btn_party
            // 
            this.btn_party.Location = new System.Drawing.Point(112, 34);
            this.btn_party.Name = "btn_party";
            this.btn_party.Size = new System.Drawing.Size(39, 17);
            this.btn_party.TabIndex = 19;
            this.btn_party.Text = "--";
            this.btn_party.UseVisualStyleBackColor = true;
            this.btn_party.Click += new System.EventHandler(this.btn_party_Click);
            // 
            // txt_party
            // 
            this.txt_party.Location = new System.Drawing.Point(112, 55);
            this.txt_party.Name = "txt_party";
            this.txt_party.Size = new System.Drawing.Size(261, 20);
            this.txt_party.TabIndex = 18;
            // 
            // txt_billno
            // 
            this.txt_billno.Location = new System.Drawing.Point(574, 56);
            this.txt_billno.Name = "txt_billno";
            this.txt_billno.Size = new System.Drawing.Size(111, 20);
            this.txt_billno.TabIndex = 18;
            // 
            // lbl_bill
            // 
            this.lbl_bill.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bill.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_bill.Location = new System.Drawing.Point(493, 56);
            this.lbl_bill.Name = "lbl_bill";
            this.lbl_bill.Size = new System.Drawing.Size(59, 19);
            this.lbl_bill.TabIndex = 17;
            this.lbl_bill.Text = "BillNo:";
            // 
            // txt_partyid
            // 
            this.txt_partyid.Location = new System.Drawing.Point(274, 11);
            this.txt_partyid.Name = "txt_partyid";
            this.txt_partyid.Size = new System.Drawing.Size(85, 20);
            this.txt_partyid.TabIndex = 14;
            // 
            // txt_invno
            // 
            this.txt_invno.Location = new System.Drawing.Point(112, 11);
            this.txt_invno.Name = "txt_invno";
            this.txt_invno.Size = new System.Drawing.Size(85, 20);
            this.txt_invno.TabIndex = 15;
            // 
            // lbl_invoice
            // 
            this.lbl_invoice.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_invoice.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_invoice.Location = new System.Drawing.Point(43, 10);
            this.lbl_invoice.Name = "lbl_invoice";
            this.lbl_invoice.Size = new System.Drawing.Size(55, 19);
            this.lbl_invoice.TabIndex = 14;
            this.lbl_invoice.Text = "InvNo:";
            // 
            // date1
            // 
            this.date1.CustomFormat = "dd-MMM-yyyy";
            this.date1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date1.Location = new System.Drawing.Point(574, 10);
            this.date1.Name = "date1";
            this.date1.Size = new System.Drawing.Size(111, 20);
            this.date1.TabIndex = 15;
            // 
            // lbl_date
            // 
            this.lbl_date.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_date.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_date.Location = new System.Drawing.Point(506, 12);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(46, 19);
            this.lbl_date.TabIndex = 14;
            this.lbl_date.Text = "Date:";
            // 
            // lbl_pname
            // 
            this.lbl_pname.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_pname.Location = new System.Drawing.Point(12, 54);
            this.lbl_pname.Name = "lbl_pname";
            this.lbl_pname.Size = new System.Drawing.Size(91, 19);
            this.lbl_pname.TabIndex = 14;
            this.lbl_pname.Text = "Party Name:";
            // 
            // lbl_partyid
            // 
            this.lbl_partyid.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_partyid.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_partyid.Location = new System.Drawing.Point(203, 11);
            this.lbl_partyid.Name = "lbl_partyid";
            this.lbl_partyid.Size = new System.Drawing.Size(65, 19);
            this.lbl_partyid.TabIndex = 14;
            this.lbl_partyid.Text = "PartyID:";
            // 
            // lbl_payment
            // 
            this.lbl_payment.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payment.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_payment.Location = new System.Drawing.Point(479, 98);
            this.lbl_payment.Name = "lbl_payment";
            this.lbl_payment.Size = new System.Drawing.Size(73, 19);
            this.lbl_payment.TabIndex = 22;
            this.lbl_payment.Text = "Payment:";
            // 
            // combo_pay
            // 
            this.combo_pay.FormattingEnabled = true;
            this.combo_pay.Items.AddRange(new object[] {
            "Cash",
            "Debit"});
            this.combo_pay.Location = new System.Drawing.Point(574, 96);
            this.combo_pay.Name = "combo_pay";
            this.combo_pay.Size = new System.Drawing.Size(111, 21);
            this.combo_pay.TabIndex = 23;
            this.combo_pay.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // panel_pm
            // 
            this.panel_pm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel_pm.Controls.Add(this.lbl_invoice);
            this.panel_pm.Controls.Add(this.combo_pay);
            this.panel_pm.Controls.Add(this.txt_partyid);
            this.panel_pm.Controls.Add(this.txt_invno);
            this.panel_pm.Controls.Add(this.lbl_payment);
            this.panel_pm.Controls.Add(this.lbl_bill);
            this.panel_pm.Controls.Add(this.lbl_description);
            this.panel_pm.Controls.Add(this.date1);
            this.panel_pm.Controls.Add(this.txt_billno);
            this.panel_pm.Controls.Add(this.txt_Description);
            this.panel_pm.Controls.Add(this.lbl_date);
            this.panel_pm.Controls.Add(this.lbl_partyid);
            this.panel_pm.Controls.Add(this.txt_party);
            this.panel_pm.Controls.Add(this.btn_party);
            this.panel_pm.Controls.Add(this.lbl_pname);
            this.panel_pm.Location = new System.Drawing.Point(13, 19);
            this.panel_pm.Margin = new System.Windows.Forms.Padding(10);
            this.panel_pm.MinimumSize = new System.Drawing.Size(689, 125);
            this.panel_pm.Name = "panel_pm";
            this.panel_pm.Size = new System.Drawing.Size(695, 125);
            this.panel_pm.TabIndex = 24;
            // 
            // ManualDG
            // 
            this.ManualDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ManualDG.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductID,
            this.PN,
            this.MU,
            this.QTY,
            this.rate,
            this.am});
            this.ManualDG.Location = new System.Drawing.Point(3, 60);
            this.ManualDG.Name = "ManualDG";
            this.ManualDG.Size = new System.Drawing.Size(682, 156);
            this.ManualDG.TabIndex = 23;
            // 
            // ProductID
            // 
            this.ProductID.HeaderText = "ProductID";
            this.ProductID.Name = "ProductID";
            this.ProductID.Width = 80;
            // 
            // PN
            // 
            this.PN.HeaderText = "ProductName";
            this.PN.Name = "PN";
            this.PN.Width = 250;
            // 
            // MU
            // 
            this.MU.HeaderText = "Munit";
            this.MU.Name = "MU";
            // 
            // QTY
            // 
            this.QTY.HeaderText = "Quantity";
            this.QTY.Name = "QTY";
            // 
            // rate
            // 
            this.rate.HeaderText = "Rate";
            this.rate.Name = "rate";
            // 
            // am
            // 
            this.am.HeaderText = "Amount";
            this.am.Name = "am";
            // 
            // btn_save
            // 
            this.btn_save.BackColor = System.Drawing.Color.White;
            this.btn_save.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_save.Location = new System.Drawing.Point(64, 226);
            this.btn_save.MinimumSize = new System.Drawing.Size(55, 29);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(55, 29);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            // 
            // txt_productname
            // 
            this.txt_productname.Location = new System.Drawing.Point(96, 35);
            this.txt_productname.Name = "txt_productname";
            this.txt_productname.Size = new System.Drawing.Size(159, 20);
            this.txt_productname.TabIndex = 1;
            // 
            // lbl_pid
            // 
            this.lbl_pid.BackColor = System.Drawing.Color.Silver;
            this.lbl_pid.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pid.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_pid.Location = new System.Drawing.Point(3, 11);
            this.lbl_pid.Name = "lbl_pid";
            this.lbl_pid.Size = new System.Drawing.Size(87, 20);
            this.lbl_pid.TabIndex = 2;
            this.lbl_pid.Text = "ProductID";
            this.lbl_pid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_pro_id
            // 
            this.lbl_pro_id.BackColor = System.Drawing.Color.Silver;
            this.lbl_pro_id.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_id.Location = new System.Drawing.Point(85, 11);
            this.lbl_pro_id.Name = "lbl_pro_id";
            this.lbl_pro_id.Size = new System.Drawing.Size(170, 20);
            this.lbl_pro_id.TabIndex = 3;
            this.lbl_pro_id.Text = "Product Name";
            // 
            // lbl_munit
            // 
            this.lbl_munit.BackColor = System.Drawing.Color.Silver;
            this.lbl_munit.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_munit.Location = new System.Drawing.Point(246, 11);
            this.lbl_munit.Name = "lbl_munit";
            this.lbl_munit.Size = new System.Drawing.Size(100, 20);
            this.lbl_munit.TabIndex = 4;
            this.lbl_munit.Text = "Munit";
            this.lbl_munit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_munit.Click += new System.EventHandler(this.lbl_munit_Click);
            // 
            // lbl_total
            // 
            this.lbl_total.BackColor = System.Drawing.Color.Transparent;
            this.lbl_total.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_total.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_total.Location = new System.Drawing.Point(370, 232);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(61, 19);
            this.lbl_total.TabIndex = 4;
            this.lbl_total.Text = "Total:";
            this.lbl_total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_qty
            // 
            this.lbl_qty.BackColor = System.Drawing.Color.Silver;
            this.lbl_qty.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_qty.Location = new System.Drawing.Point(328, 11);
            this.lbl_qty.Name = "lbl_qty";
            this.lbl_qty.Size = new System.Drawing.Size(94, 20);
            this.lbl_qty.TabIndex = 5;
            this.lbl_qty.Text = "Quantity";
            this.lbl_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_prate
            // 
            this.lbl_prate.BackColor = System.Drawing.Color.Silver;
            this.lbl_prate.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_prate.Location = new System.Drawing.Point(415, 11);
            this.lbl_prate.Name = "lbl_prate";
            this.lbl_prate.Size = new System.Drawing.Size(97, 20);
            this.lbl_prate.TabIndex = 6;
            this.lbl_prate.Text = "P_Rate";
            this.lbl_prate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_productid
            // 
            this.txt_productid.Location = new System.Drawing.Point(26, 35);
            this.txt_productid.Name = "txt_productid";
            this.txt_productid.Size = new System.Drawing.Size(64, 20);
            this.txt_productid.TabIndex = 7;
            // 
            // lbl_amount
            // 
            this.lbl_amount.BackColor = System.Drawing.Color.Silver;
            this.lbl_amount.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_amount.Location = new System.Drawing.Point(596, 11);
            this.lbl_amount.Name = "lbl_amount";
            this.lbl_amount.Size = new System.Drawing.Size(89, 20);
            this.lbl_amount.TabIndex = 6;
            this.lbl_amount.Text = "Amount";
            this.lbl_amount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_munit
            // 
            this.txt_munit.Location = new System.Drawing.Point(261, 35);
            this.txt_munit.Name = "txt_munit";
            this.txt_munit.Size = new System.Drawing.Size(76, 20);
            this.txt_munit.TabIndex = 8;
            // 
            // txt_qty
            // 
            this.txt_qty.Location = new System.Drawing.Point(343, 35);
            this.txt_qty.Name = "txt_qty";
            this.txt_qty.Size = new System.Drawing.Size(79, 20);
            this.txt_qty.TabIndex = 9;
            this.txt_qty.Text = "0";
            this.txt_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_prate
            // 
            this.txt_prate.Location = new System.Drawing.Point(428, 35);
            this.txt_prate.Name = "txt_prate";
            this.txt_prate.Size = new System.Drawing.Size(86, 20);
            this.txt_prate.TabIndex = 9;
            this.txt_prate.Text = "0";
            this.txt_prate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_tqty
            // 
            this.txt_tqty.Location = new System.Drawing.Point(428, 232);
            this.txt_tqty.Name = "txt_tqty";
            this.txt_tqty.ReadOnly = true;
            this.txt_tqty.Size = new System.Drawing.Size(87, 20);
            this.txt_tqty.TabIndex = 9;
            this.txt_tqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_amount
            // 
            this.txt_amount.Enabled = false;
            this.txt_amount.Location = new System.Drawing.Point(604, 34);
            this.txt_amount.Name = "txt_amount";
            this.txt_amount.Size = new System.Drawing.Size(81, 20);
            this.txt_amount.TabIndex = 10;
            this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_tamount
            // 
            this.txt_tamount.Location = new System.Drawing.Point(594, 233);
            this.txt_tamount.Name = "txt_tamount";
            this.txt_tamount.ReadOnly = true;
            this.txt_tamount.Size = new System.Drawing.Size(84, 20);
            this.txt_tamount.TabIndex = 10;
            this.txt_tamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.White;
            this.btn_delete.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Bold);
            this.btn_delete.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_delete.Location = new System.Drawing.Point(125, 226);
            this.btn_delete.MinimumSize = new System.Drawing.Size(60, 29);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(60, 29);
            this.btn_delete.TabIndex = 11;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = false;
            // 
            // btn_reset
            // 
            this.btn_reset.BackColor = System.Drawing.Color.White;
            this.btn_reset.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Bold);
            this.btn_reset.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_reset.Location = new System.Drawing.Point(189, 226);
            this.btn_reset.MinimumSize = new System.Drawing.Size(55, 29);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(55, 29);
            this.btn_reset.TabIndex = 12;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = false;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_max
            // 
            this.btn_max.Location = new System.Drawing.Point(3, 34);
            this.btn_max.Name = "btn_max";
            this.btn_max.Size = new System.Drawing.Size(21, 20);
            this.btn_max.TabIndex = 20;
            this.btn_max.Text = "--";
            this.btn_max.UseVisualStyleBackColor = true;
            this.btn_max.Click += new System.EventHandler(this.btn_max_Click);
            // 
            // btn_new
            // 
            this.btn_new.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn_new.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_new.Location = new System.Drawing.Point(3, 226);
            this.btn_new.MinimumSize = new System.Drawing.Size(55, 29);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(55, 29);
            this.btn_new.TabIndex = 21;
            this.btn_new.Text = "New";
            this.btn_new.UseVisualStyleBackColor = false;
            // 
            // lbl_amou
            // 
            this.lbl_amou.BackColor = System.Drawing.Color.Transparent;
            this.lbl_amou.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_amou.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_amou.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_amou.Location = new System.Drawing.Point(521, 233);
            this.lbl_amou.Name = "lbl_amou";
            this.lbl_amou.Size = new System.Drawing.Size(80, 19);
            this.lbl_amou.TabIndex = 22;
            this.lbl_amou.Text = "Amount:";
            this.lbl_amou.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_print
            // 
            this.btn_print.BackColor = System.Drawing.Color.White;
            this.btn_print.Font = new System.Drawing.Font("Sitka Text", 9.749999F, System.Drawing.FontStyle.Bold);
            this.btn_print.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_print.Location = new System.Drawing.Point(250, 226);
            this.btn_print.MinimumSize = new System.Drawing.Size(55, 29);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(55, 29);
            this.btn_print.TabIndex = 24;
            this.btn_print.Text = "Print";
            this.btn_print.UseVisualStyleBackColor = false;
            // 
            // lbl_ptax
            // 
            this.lbl_ptax.BackColor = System.Drawing.Color.Silver;
            this.lbl_ptax.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ptax.Location = new System.Drawing.Point(506, 11);
            this.lbl_ptax.Name = "lbl_ptax";
            this.lbl_ptax.Size = new System.Drawing.Size(98, 20);
            this.lbl_ptax.TabIndex = 25;
            this.lbl_ptax.Text = "P_Tax";
            this.lbl_ptax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_ptax
            // 
            this.txt_ptax.Location = new System.Drawing.Point(520, 35);
            this.txt_ptax.Name = "txt_ptax";
            this.txt_ptax.Size = new System.Drawing.Size(78, 20);
            this.txt_ptax.TabIndex = 26;
            this.txt_ptax.Text = "0";
            this.txt_ptax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel3.Controls.Add(this.lbl_pro_id);
            this.panel3.Controls.Add(this.lbl_pid);
            this.panel3.Controls.Add(this.lbl_prate);
            this.panel3.Controls.Add(this.txt_ptax);
            this.panel3.Controls.Add(this.txt_qty);
            this.panel3.Controls.Add(this.lbl_qty);
            this.panel3.Controls.Add(this.btn_max);
            this.panel3.Controls.Add(this.btn_new);
            this.panel3.Controls.Add(this.lbl_total);
            this.panel3.Controls.Add(this.txt_tamount);
            this.panel3.Controls.Add(this.lbl_amount);
            this.panel3.Controls.Add(this.txt_munit);
            this.panel3.Controls.Add(this.lbl_munit);
            this.panel3.Controls.Add(this.txt_productid);
            this.panel3.Controls.Add(this.ManualDG);
            this.panel3.Controls.Add(this.btn_save);
            this.panel3.Controls.Add(this.lbl_amou);
            this.panel3.Controls.Add(this.txt_prate);
            this.panel3.Controls.Add(this.btn_delete);
            this.panel3.Controls.Add(this.txt_tqty);
            this.panel3.Controls.Add(this.btn_print);
            this.panel3.Controls.Add(this.btn_reset);
            this.panel3.Controls.Add(this.txt_amount);
            this.panel3.Controls.Add(this.lbl_ptax);
            this.panel3.Controls.Add(this.txt_productname);
            this.panel3.Location = new System.Drawing.Point(13, 146);
            this.panel3.Margin = new System.Windows.Forms.Padding(10);
            this.panel3.MaximumSize = new System.Drawing.Size(838, 355);
            this.panel3.MinimumSize = new System.Drawing.Size(689, 262);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(695, 262);
            this.panel3.TabIndex = 27;
            // 
            // Purchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(714, 413);
            this.Controls.Add(this.panel_pm);
            this.Controls.Add(this.panel3);
            this.MaximumSize = new System.Drawing.Size(879, 550);
            this.MinimumSize = new System.Drawing.Size(730, 452);
            this.Name = "Purchase";
            this.Text = "Purchase";
            this.Load += new System.EventHandler(this.Purchase_Load);
            this.ResizeEnd += new System.EventHandler(this.Purchase_ResizeEnd);
            this.panel_pm.ResumeLayout(false);
            this.panel_pm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ManualDG)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_description;
        private System.Windows.Forms.TextBox txt_Description;
        private System.Windows.Forms.Button btn_party;
        private System.Windows.Forms.TextBox txt_party;
        private System.Windows.Forms.TextBox txt_billno;
        private System.Windows.Forms.Label lbl_bill;
        private System.Windows.Forms.TextBox txt_partyid;
        private System.Windows.Forms.TextBox txt_invno;
        private System.Windows.Forms.Label lbl_invoice;
        private System.Windows.Forms.DateTimePicker date1;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_pname;
        private System.Windows.Forms.Label lbl_partyid;
        private System.Windows.Forms.ComboBox combo_pay;
        private System.Windows.Forms.Label lbl_payment;
        private System.Windows.Forms.Panel panel_pm;
        private System.Windows.Forms.DataGridView ManualDG;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PN;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn am;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox txt_productname;
        private System.Windows.Forms.Label lbl_pid;
        private System.Windows.Forms.Label lbl_pro_id;
        private System.Windows.Forms.Label lbl_munit;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_qty;
        private System.Windows.Forms.Label lbl_prate;
        private System.Windows.Forms.TextBox txt_productid;
        private System.Windows.Forms.Label lbl_amount;
        private System.Windows.Forms.TextBox txt_munit;
        private System.Windows.Forms.TextBox txt_qty;
        private System.Windows.Forms.TextBox txt_prate;
        private System.Windows.Forms.TextBox txt_tqty;
        private System.Windows.Forms.TextBox txt_amount;
        private System.Windows.Forms.TextBox txt_tamount;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_max;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Label lbl_amou;
        private System.Windows.Forms.Button btn_print;
        private System.Windows.Forms.Label lbl_ptax;
        private System.Windows.Forms.TextBox txt_ptax;
        private System.Windows.Forms.Panel panel3;
    }
}