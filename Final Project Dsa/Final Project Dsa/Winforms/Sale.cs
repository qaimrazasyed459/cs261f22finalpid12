﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa
{
    public partial class Sale : Form
    {
        public Sale()
        {
            InitializeComponent();
        }

        private void Sale_Load(object sender, EventArgs e)
        {

        }

        public void MaxUI()
        {
                panel_sd.Size = new Size(838, 355);

                panel_sm.Size = new Size(838, 146);



                date1.Location = new Point(703, 9);
                date1.Size = new Size(121, 20);
                lbl_date.Size = new Size(46, 19);
                lbl_date.Location = new Point(646, 9);


                txt_billno.Location = new Point(703, 55);
                txt_billno.Size = new Size(121, 20);
                lbl_bill.Size = new Size(59, 19);
                lbl_bill.Location = new Point(633, 53);

                combo_pay.Location = new Point(714, 93);
                combo_pay.Size = new Size(121, 20);
                lbl_payment.Size = new Size(73, 19);
                lbl_payment.Location = new Point(619, 95);



                lbl_pid.Location = new Point(34, 10);
                lbl_pid.Size = new Size(87, 21);
                lbl_pid.Font = new Font("Sitka Small", 11);


                lbl_pro_id.Location = new Point(118, 10);
                lbl_pro_id.Size = new Size(192, 21);
                lbl_pro_id.Font = new Font("Sitka Small", 11);

                lbl_munit.Location = new Point(306, 10);
                lbl_munit.Size = new Size(100, 21);
                lbl_munit.Font = new Font("Sitka Small", 11);


                lbl_qty.Location = new Point(403, 10);
                lbl_qty.Size = new Size(110, 21);
                lbl_qty.Font = new Font("Sitka Small", 11);

                lbl_srate.Location = new Point(506, 10);
                lbl_srate.Size = new Size(97, 21);
                lbl_srate.Font = new Font("Sitka Small", 11);

                lbl_stax.Location = new Point(598, 10);
                lbl_stax.Size = new Size(109, 21);
                lbl_stax.Font = new Font("Sitka Small", 11);

                lbl_amount.Location = new Point(704, 10);
                lbl_amount.Size = new Size(108, 21);
                lbl_amount.Font = new Font("Sitka Small", 11);



                txt_productid.Location = new Point(38, 43);
                txt_productid.Size = new Size(77, 20);

                txt_productname.Location = new Point(121, 43);
                txt_productname.Size = new Size(189, 20);

                txt_munit.Location = new Point(316, 43);
                txt_munit.Size = new Size(97, 20);

                txt_qty.Location = new Point(419, 43);
                txt_qty.Size = new Size(97, 20);

                txt_srate.Location = new Point(522, 43);
                txt_srate.Size = new Size(86, 20);

                txt_stax.Location = new Point(614, 43);
                txt_stax.Size = new Size(92, 20);

                txt_amount.Location = new Point(713, 43);
                txt_amount.Size = new Size(102, 20);

                ManualSDG.Location = new Point(6, 69);
                ManualSDG.Size = new Size(819, 180);

                btn_new.Location = new Point(7, 260);
                btn_new.Size = new Size(70, 29);

                btn_save.Location = new Point(81, 260);
                btn_save.Size = new Size(70, 29);

                btn_delete.Location = new Point(157, 260);
                btn_delete.Size = new Size(70, 29);

                btn_reset.Location = new Point(233, 260);
                btn_reset.Size = new Size(70, 29);

                btn_print.Location = new Point(309, 260);
                btn_print.Size = new Size(70, 29);

                lbl_total.Location = new Point(511, 264);
                txt_tqty.Location = new Point(569, 264);
                lbl_amou.Location = new Point(662, 265);
                txt_tamount.Location = new Point(735, 265);
            }

        private void btn_max_Click(object sender, EventArgs e)
        {
            MaxUI();
        }
    }
}
