﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa
{
    public partial class Main_Menu : Form
    {
        bool slidebarexpand;
        public Main_Menu()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            slide_time.Start();
        }

        private void lbl_Projectname_Click(object sender, EventArgs e)
        {

        }

        private void Slide_Time_Tick(object sender, EventArgs e)
        {
            if (slidebarexpand)
            {
                panel_main.Width -= 10;
                if (panel_main.Width == panel_main.MinimumSize.Width)
                {
                    slidebarexpand = false;
                   slide_time.Stop();
                }
            }
            else
            {
                panel_main.Width += 10;
                if (panel_main.Width == panel_main.MaximumSize.Width)
                {
                    slidebarexpand = true;
                    slide_time.Stop();
                }
            }
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {
            //if (MaximizeBox)
            //{
                //panel_main.MinimumSize = new Size(181, 427);
              //  panel_main.MaximumSize = new Size(265, 783);
                // set the max and min of the button and pane
            //}
        }

        private void lbl_mode_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= true;
          /*  if (MaximizeBox)
            {
                mode_panel.MaximumSize = new Size(209, 198);
                btn_Cashier.MaximumSize = new Size(198, 45);
                btn_Cashier.Location = new Point(5, 12);

                btn_Manager.MaximumSize = new Size(198, 45);
                btn_Manager.Location = new Point(5, 59);

                btn_Rider.MaximumSize = new Size(198, 45);
                btn_Rider.Location = new Point(5, 104);

                btn_shopkeeper.MaximumSize = new Size(198, 45);
                btn_shopkeeper.Location = new Point(5, 146);
            }*/
        }

        private void btn_Manager_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void btn_Cashier_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
            Form Cashier = new Cashier();
            Cashier.Show();
        }

        private void btn_Rider_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void btn_shopkeeper_Click(object sender, EventArgs e)
        {
            mode_panel.Visible= false;
        }

        private void mode_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
