﻿
namespace Final_Project_Dsa.Winforms.Manager
{
    partial class Email
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_cfill = new System.Windows.Forms.GroupBox();
            this.lbl_companyid = new System.Windows.Forms.Label();
            this.lbl_companyname = new System.Windows.Forms.Label();
            this.txt_description = new System.Windows.Forms.TextBox();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.lbl_description = new System.Windows.Forms.Label();
            this.btn_send = new System.Windows.Forms.Button();
            this.txt_cname = new System.Windows.Forms.TextBox();
            this.grp_cfill.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_cfill
            // 
            this.grp_cfill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grp_cfill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.grp_cfill.Controls.Add(this.lbl_companyid);
            this.grp_cfill.Controls.Add(this.lbl_companyname);
            this.grp_cfill.Controls.Add(this.txt_description);
            this.grp_cfill.Controls.Add(this.txt_id);
            this.grp_cfill.Controls.Add(this.lbl_description);
            this.grp_cfill.Controls.Add(this.btn_send);
            this.grp_cfill.Controls.Add(this.txt_cname);
            this.grp_cfill.Location = new System.Drawing.Point(15, 32);
            this.grp_cfill.MinimumSize = new System.Drawing.Size(674, 163);
            this.grp_cfill.Name = "grp_cfill";
            this.grp_cfill.Size = new System.Drawing.Size(675, 329);
            this.grp_cfill.TabIndex = 2;
            this.grp_cfill.TabStop = false;
            this.grp_cfill.Enter += new System.EventHandler(this.grp_cfill_Enter);
            // 
            // lbl_companyid
            // 
            this.lbl_companyid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_companyid.AutoSize = true;
            this.lbl_companyid.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyid.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_companyid.Location = new System.Drawing.Point(70, 31);
            this.lbl_companyid.Name = "lbl_companyid";
            this.lbl_companyid.Size = new System.Drawing.Size(48, 19);
            this.lbl_companyid.TabIndex = 18;
            this.lbl_companyid.Text = "From:";
            // 
            // lbl_companyname
            // 
            this.lbl_companyname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_companyname.AutoSize = true;
            this.lbl_companyname.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_companyname.Location = new System.Drawing.Point(70, 67);
            this.lbl_companyname.Name = "lbl_companyname";
            this.lbl_companyname.Size = new System.Drawing.Size(30, 19);
            this.lbl_companyname.TabIndex = 19;
            this.lbl_companyname.Text = "To:";
            // 
            // txt_description
            // 
            this.txt_description.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_description.Location = new System.Drawing.Point(124, 125);
            this.txt_description.Multiline = true;
            this.txt_description.Name = "txt_description";
            this.txt_description.Size = new System.Drawing.Size(421, 146);
            this.txt_description.TabIndex = 27;
            // 
            // txt_id
            // 
            this.txt_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_id.Location = new System.Drawing.Point(124, 30);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(145, 20);
            this.txt_id.TabIndex = 20;
            // 
            // lbl_description
            // 
            this.lbl_description.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_description.AutoSize = true;
            this.lbl_description.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_description.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_description.Location = new System.Drawing.Point(28, 142);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(90, 19);
            this.lbl_description.TabIndex = 26;
            this.lbl_description.Text = "Description:";
            // 
            // btn_send
            // 
            this.btn_send.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_send.Font = new System.Drawing.Font("Sitka Small", 9.75F, System.Drawing.FontStyle.Bold);
            this.btn_send.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_send.Location = new System.Drawing.Point(549, 286);
            this.btn_send.MinimumSize = new System.Drawing.Size(66, 27);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(66, 27);
            this.btn_send.TabIndex = 24;
            this.btn_send.Text = "Send";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // txt_cname
            // 
            this.txt_cname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_cname.Location = new System.Drawing.Point(124, 66);
            this.txt_cname.Name = "txt_cname";
            this.txt_cname.Size = new System.Drawing.Size(145, 20);
            this.txt_cname.TabIndex = 22;
            // 
            // Email
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(705, 411);
            this.Controls.Add(this.grp_cfill);
            this.MinimumSize = new System.Drawing.Size(721, 450);
            this.Name = "Email";
            this.Text = "Email";
            this.grp_cfill.ResumeLayout(false);
            this.grp_cfill.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grp_cfill;
        private System.Windows.Forms.Label lbl_companyid;
        private System.Windows.Forms.Label lbl_companyname;
        private System.Windows.Forms.TextBox txt_description;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.Label lbl_description;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.TextBox txt_cname;
    }
}