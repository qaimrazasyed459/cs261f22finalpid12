﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa
{
    public partial class Pharmacy : Form
    {
        bool slidebarexpand;
        public Pharmacy()
        {
            InitializeComponent();
        }

        private void Menu_pic_Click(object sender, EventArgs e)
        {
           
        }

        private void panel_cashier_Paint(object sender, PaintEventArgs e)
        {

        }

        private void slide_timer2_Tick(object sender, EventArgs e)
        {
            if (slidebarexpand)
            {
                panel_main.Width -= 10;
                if (panel_main.Width == panel_main.MinimumSize.Width)
                {
                    slidebarexpand = false;
                    slide_timer2.Stop();
                }
            }
            else
            {
                panel_main.Width += 10;
                if (panel_main.Width == panel_main.MaximumSize.Width)
                {
                    slidebarexpand = true;
                    slide_timer2.Stop();
                }
            }
        }

        private void btn_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Menu_pic_Click_1(object sender, EventArgs e)
        {

        }

        private void btn_panel_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void Menu_pic_Click_2(object sender, EventArgs e)
        {
            slide_timer2.Start();
        }

        private void Pharmacy_Load(object sender, EventArgs e)
        {
            if (MaximizeBox)
            {
                //    panel_main.MinimumSize = new Size(184, 427);
                //  panel_main.MaximumSize = new Size(265, 783);
                btn_panel.Size = new Size(879, 450);
            }
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            btn_panel.Controls.Clear();
            Form ReturnPharmacy = new ReturnPharmacy();

            ReturnPharmacy.Size = btn_panel.Size;
            ReturnPharmacy.TopLevel = false;
            ReturnPharmacy.Parent = btn_panel;
            ReturnPharmacy.FormBorderStyle = FormBorderStyle.None;
            panel_main.Width = MinimumSize.Width;
            //pictureBox2.Visible = false;
            ReturnPharmacy.Show();
        }
    }
}
