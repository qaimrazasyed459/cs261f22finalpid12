﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_Project_Dsa.Winforms.Manager
{
    public partial class Employ_Detail : Form
    {
        public Employ_Detail()
        {
            InitializeComponent();
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string query = "SELECT * FROM tblHireEmploys WHERE ID = " + id1;
                SqlCommand cmd = new SqlCommand(query, con);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    txt_name.Text = reader["FirstName"].ToString();
                    txt_address.Text = reader["Address"].ToString();
                    txt_email.Text = reader["Email"].ToString();
                    txt_phonenumber.Text = reader["PhoneNumber"].ToString();
                    txt_salary.Text = reader["Salary"].ToString();
                    txt_hiredate.Text = reader["HireDate"].ToString();
                }
                else
                {
                    MessageBox.Show("No record found");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            try
            {
                string conString = "Data Source=.;Initial Catalog=Manager;Integrated Security=True";
                SqlConnection con = new SqlConnection(conString);
                con.Open();
                string id1 = txt_id.Text;
                string n1 = txt_name.Text;
                string address = txt_address.Text;
                string email = txt_email.Text;
                string phone = txt_phonenumber.Text;
                string salary = txt_salary.Text; //reader["Salary"].ToString();
                string date = txt_hiredate.Text;
                string query = "UPDATE tblHireEmploys SET FirstName = '" + n1 + "', Address = '" + address + "',  Email = '" + email + "', PhoneNumber=" + phone + ",Salary=" + salary + ", HireDate='" + date + "' where ID=" + id1;
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("saved");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }
    }
}
